﻿/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 10                       */
/* Created on:     04/05/2015 14:50:09                          */
/*==============================================================*/




/*==============================================================*/
/* Table: AGAMA                                                 */
/*==============================================================*/
create table AGAMA 
(
   ID_AGAMA             char(2)                        not null,
   NAMA_AGAMA           varchar(20)                    not null,
   constraint PK_AGAMA primary key (ID_AGAMA)
);

/*==============================================================*/
/* Index: AGAMA_PK                                              */
/*==============================================================*/
create unique index AGAMA_PK on AGAMA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Table: KK                                                    */
/*==============================================================*/
create table KK 
(
   NOMOR_KK             varchar (36)                   not null,
   ALAMAT_KK            varchar(500)                   not null,
   constraint PK_KK primary key (NOMOR_KK)
);

/*==============================================================*/
/* Index: KK_PK                                                 */
/*==============================================================*/
create unique index KK_PK on KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: KTP                                                   */
/*==============================================================*/
create table KTP 
(
   NOMOR_REGISTRASI     varchar (36)                   not null,
   UNIQUE_ID_WN         varchar (36)                   not null,
   ID_MUTASI_MASUK      varchar (36)                   null,
   ALAMAT               varchar(500)                   not null,
   BERLAKU_HINGGA       date                           not null,
   FOTO                 varchar(64000)                  null,
   STATUS_BERLAKU       smallint                       not null,
   constraint PK_KTP primary key (NOMOR_REGISTRASI)
);

/*==============================================================*/
/* Index: KTP_PK                                                */
/*==============================================================*/
create unique index KTP_PK on KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on KTP (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_14_FK on KTP (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Table: LEVEL_WILAYAH                                         */
/*==============================================================*/
create table LEVEL_WILAYAH 
(
   ID_LEVEL_WILAYAH     integer                        not null,
   NAMA_LEVEL_WILAYAH   varchar(50)                    not null,
   constraint PK_LEVEL_WILAYAH primary key (ID_LEVEL_WILAYAH)
);

/*==============================================================*/
/* Index: LEVEL_WILAYAH_PK                                      */
/*==============================================================*/
create unique index LEVEL_WILAYAH_PK on LEVEL_WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Table: LOG_KK                                                */
/*==============================================================*/
create table LOG_KK 
(
   ID_LOG_KK            integer                        not null,
   NOMOR_KK             varchar (36)                   not null,
   ALAMAT_LOG_KK        varchar(500)                   not null,
   constraint PK_LOG_KK primary key (ID_LOG_KK)
);

/*==============================================================*/
/* Index: LOG_KK_PK                                             */
/*==============================================================*/
create unique index LOG_KK_PK on LOG_KK (
ID_LOG_KK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_18_FK on LOG_KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: LOG_KTP                                               */
/*==============================================================*/
create table LOG_KTP 
(
   ID_LOG_KTP           integer                        not null,
   NOMOR_REGISTRASI     varchar (36)                   not null,
   ALAMAT_LOG           varchar(500)                   not null,
   BERLAKU_HINGGA_LOG   date                           not null,
   FOTO_LOG             varchar(64000)                  null,
   STATUS_BERLAKU_LOG   smallint                       not null,
   constraint PK_LOG_KTP primary key (ID_LOG_KTP)
);

/*==============================================================*/
/* Index: LOG_KTP_PK                                            */
/*==============================================================*/
create unique index LOG_KTP_PK on LOG_KTP (
ID_LOG_KTP ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_11_FK on LOG_KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: LOG_WARGA_NEGARA                                      */
/*==============================================================*/
create table LOG_WARGA_NEGARA 
(
   ID_LOG_WARGA_NEGARA  integer                        not null,
   UNIQUE_ID_WN         varchar (36)                   not null,
   NAMA_LOG             varchar(250)                   not null,
   JENIS_KELAMIN_LOG    char(1)                        not null,
   GOLONGAN_DARAH_LOG   char(2)                        not null,
   STATUS_KAWIN_LOG     char(1)                        not null,
   STATUS_HIDUP_LOG     smallint                       not null,
   STATUS_HUBUNGAN_DALAM_KELUARGA__LOG integer                        not null,
   constraint PK_LOG_WARGA_NEGARA primary key (ID_LOG_WARGA_NEGARA)
);

/*==============================================================*/
/* Index: LOG_WARGA_NEGARA_PK                                   */
/*==============================================================*/
create unique index LOG_WARGA_NEGARA_PK on LOG_WARGA_NEGARA (
ID_LOG_WARGA_NEGARA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_15_FK on LOG_WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Table: MUTASI                                                */
/*==============================================================*/
create table MUTASI 
(
   ID_MUTASI_MASUK      varchar (36)                   not null,
   NOMOR_REGISTRASI     varchar (36)                   not null,
   TANGGAL_KELUAR       date                           null,
   TANGGAL_MASUK        date                           null,
   constraint PK_MUTASI primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Index: MUTASI_PK                                             */
/*==============================================================*/
create unique index MUTASI_PK on MUTASI (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_12_FK on MUTASI (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: PEKERJAAN                                             */
/*==============================================================*/
create table PEKERJAAN 
(
   ID_PEKERJAAN         char(3)                        not null,
   NAMA_PEKERJAAN       varchar(20)                    not null,
   constraint PK_PEKERJAAN primary key (ID_PEKERJAAN)
);

/*==============================================================*/
/* Index: PEKERJAAN_PK                                          */
/*==============================================================*/
create unique index PEKERJAAN_PK on PEKERJAAN (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Table: PENDIDIKAN                                            */
/*==============================================================*/
create table PENDIDIKAN 
(
   ID_PENDIDIKAN        char(2)                        not null,
   NAMA_PENDIDIKAN      varchar(50)                    not null,
   constraint PK_PENDIDIKAN primary key (ID_PENDIDIKAN)
);

/*==============================================================*/
/* Index: PENDIDIKAN_PK                                         */
/*==============================================================*/
create unique index PENDIDIKAN_PK on PENDIDIKAN (
ID_PENDIDIKAN ASC
);

/*==============================================================*/
/* Table: WARGA_NEGARA                                          */
/*==============================================================*/
create table WARGA_NEGARA 
(
   UNIQUE_ID_WN         varchar (36)                   not null,
   ID_PENDIDIKAN        char(2)                        not null,
   ID_AGAMA             char(2)                        not null,
   ID_PEKERJAAN         char(3)                        not null,
   NOMOR_KK             varchar (36)                   null,
   NIK                  char(16)                       not null,
   NAMA                 varchar(250)                   not null,
   TANGGAL_LAHIR        date                           not null,
   JENIS_KELAMIN        char(1)                        not null,
   GOLONGAN_DARAH       char(2)                        not null,
   STATUS_KAWIN         char(1)                        null,
   STATUS_HIDUP         smallint                       not null,
   SHDK2                integer                        not null,
   NAMA_AYAH            varchar(250)                   not null,
   NAMA_IBU             varchar(250)                   not null,
   constraint PK_WARGA_NEGARA primary key (UNIQUE_ID_WN)
);

/*==============================================================*/
/* Index: WARGA_NEGARA_PK                                       */
/*==============================================================*/
create unique index WARGA_NEGARA_PK on WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_20_FK on WARGA_NEGARA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_21_FK on WARGA_NEGARA (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_1_FK on WARGA_NEGARA (
NOMOR_KK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_10_FK on WARGA_NEGARA (
ID_PENDIDIKAN ASC
);

/*==============================================================*/
/* Table: WILAYAH                                               */
/*==============================================================*/
create table WILAYAH 
(
   ID_WILAYAH           integer                        not null,
   WIL_ID_WILAYAH       integer                        null,
   ID_LEVEL_WILAYAH     integer                        not null,
   NAMA_WILAYAH         varchar(50)                    not null,
   constraint PK_WILAYAH primary key (ID_WILAYAH)
);

/*==============================================================*/
/* Index: WILAYAH_PK                                            */
/*==============================================================*/
create unique index WILAYAH_PK on WILAYAH (
ID_WILAYAH ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_13_FK on WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Index: INDUK_FK                                              */
/*==============================================================*/
create index INDUK_FK on WILAYAH (
WIL_ID_WILAYAH ASC
);

alter table KTP
   add constraint FK_KTP_RELATIONS_MUTASI foreign key (ID_MUTASI_MASUK)
      references MUTASI (ID_MUTASI_MASUK)
      on update restrict
      on delete restrict;

alter table KTP
   add constraint FK_KTP_RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table LOG_KK
   add constraint FK_LOG_KK_RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table LOG_KTP
   add constraint FK_LOG_KTP_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table LOG_WARGA_NEGARA
   add constraint FK_LOG_WARG_RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table MUTASI
   add constraint FK_MUTASI_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_PENDIDIK foreign key (ID_PENDIDIKAN)
      references PENDIDIKAN (ID_PENDIDIKAN)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_AGAMA foreign key (ID_AGAMA)
      references AGAMA (ID_AGAMA)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_PEKERJAA foreign key (ID_PEKERJAAN)
      references PEKERJAAN (ID_PEKERJAAN)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_INDUK_WILAYAH foreign key (WIL_ID_WILAYAH)
      references WILAYAH (ID_WILAYAH)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_RELATIONS_LEVEL_WI foreign key (ID_LEVEL_WILAYAH)
      references LEVEL_WILAYAH (ID_LEVEL_WILAYAH)
      on update restrict
      on delete restrict;


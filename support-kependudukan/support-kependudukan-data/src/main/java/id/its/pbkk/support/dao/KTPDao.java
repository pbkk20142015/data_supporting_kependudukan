package id.its.pbkk.support.dao;

import id.its.pbkk.support.domain.KTP;

import java.util.List;

public interface KTPDao {
	
	public List<KTP> list();
	public KTP findById(String id);
	public void save(KTP ktp);
	public void delete(KTP ktp);

}

package id.its.pbkk.support.dao;

import id.its.pbkk.support.domain.LogKk;

import java.util.List;

public interface LogKkDao {
	public List<LogKk> list();
	public LogKk findById(int id);
	public void save(LogKk logkk);
	public void delete(LogKk logkk);

}

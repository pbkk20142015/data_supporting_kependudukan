package id.its.pbkk.support.dao.impl;

import id.its.pbkk.support.dao.MutasiDao;
import id.its.pbkk.support.domain.Mutasi;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("mutasiDao")
public class HibMutasiDao implements MutasiDao {
private SessionFactory sessionFactory;
	
	public HibMutasiDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Transactional(readOnly=true)
	public List<Mutasi> list() {
		List<Mutasi> mutasi = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Mutasi")
				.list();
		return mutasi;
	}

	@Transactional(readOnly=true)
	public Mutasi findById(String id) {
		Mutasi mutasi = (Mutasi) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Mutasi c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		return mutasi;
	}

	public void save(Mutasi mutasi) {
		this.getSessionFactory().getCurrentSession().save(mutasi);
		
	}

	public void delete(Mutasi mutasi) {
		this.getSessionFactory().getCurrentSession().delete(mutasi);
		
	}
}

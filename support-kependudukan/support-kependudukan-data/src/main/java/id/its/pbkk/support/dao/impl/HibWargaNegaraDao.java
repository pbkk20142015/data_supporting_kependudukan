package id.its.pbkk.support.dao.impl;

import id.its.pbkk.support.dao.WargaNegaraDao;
import id.its.pbkk.support.domain.WargaNegara;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("wargaNegaraDao")
public class HibWargaNegaraDao implements WargaNegaraDao {
	
	private SessionFactory sessionFactory;
	
	public HibWargaNegaraDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<WargaNegara> list() {
		List<WargaNegara> warganegara = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from WargaNegara")
				.list();
		return warganegara;
	}

	@Transactional(readOnly=true)
	public WargaNegara findById(String id) {
		WargaNegara warganegara = (WargaNegara) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from WargaNegara c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return warganegara;
	}

	public void save(WargaNegara wargaNegara) {
		this.getSessionFactory().getCurrentSession().save(wargaNegara);
		
	}

	public void delete(WargaNegara wargaNegara) {
		this.getSessionFactory().getCurrentSession().delete(wargaNegara);
		
	}

	@Transactional(readOnly=true)
	public List<WargaNegara> findByNik(String nik) {
		List<WargaNegara> warganegara = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from WargaNegara c where c.nik like :nik")
				.setParameter("nik", "%" + nik + "%")
				.list();	
		return warganegara;
	}

}

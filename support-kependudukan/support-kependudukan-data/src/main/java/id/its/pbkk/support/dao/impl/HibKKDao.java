package id.its.pbkk.support.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.support.dao.KKDao;
import id.its.pbkk.support.domain.KK;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository("kKDao")
public class HibKKDao implements KKDao {
	
	private SessionFactory sessionFactory;
	
	public HibKKDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Transactional(readOnly=true)
	public List<KK> list() {
		List<KK> kk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from KK")
				.list();
		return kk;
	}

	@Transactional(readOnly=true)
	public KK findByNomor_kk(String nomor_kk) {
		KK kk = (KK) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from KK c where c.nomor_kk like :nomor_kk")
				.setParameter("nomor_kk", "%" + nomor_kk + "%");
		return kk;
	}

	public void save(KK kK) {
		this.getSessionFactory().getCurrentSession().save(kK);
		
	}

	public void delete(KK kK) {
		this.getSessionFactory().getCurrentSession().delete(kK);
		
	}
	

}

package id.its.pbkk.support.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.support.dao.LogKtpDao;
import id.its.pbkk.support.domain.LogKtp;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("logKtpDao")
public class HibLogKtpDao implements LogKtpDao{
private SessionFactory sessionFactory;
	
	public HibLogKtpDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Transactional(readOnly=true)
	public List<LogKtp> list() {
		List<LogKtp> logktp = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogKtp")
				.list();
		return logktp;
	}

	@Transactional(readOnly=true)
	public LogKtp findById(int id) {
		LogKtp logktp = (LogKtp) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogKtp c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		return logktp;
	}

	public void save(LogKtp logKtp) {
		this.getSessionFactory().getCurrentSession().save(logKtp);
		
	}

	public void delete(LogKtp logKtp) {
		this.getSessionFactory().getCurrentSession().delete(logKtp);
		
	}

}

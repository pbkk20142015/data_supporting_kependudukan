package id.its.pbkk.support.dao.impl;

import id.its.pbkk.support.dao.LogWargaNegaraDao;
import id.its.pbkk.support.domain.LogWargaNegara;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("logWargaNegaraDao")
public class HibLogWargaNegaraDao implements LogWargaNegaraDao {
private SessionFactory sessionFactory;
	
	public HibLogWargaNegaraDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<LogWargaNegara> list() {
		List<LogWargaNegara> logwarganegara = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogWargaNegara")
				.list();
		return logwarganegara;
	}

	@Transactional(readOnly=true)
	public LogWargaNegara findById(int id) {
		LogWargaNegara logwarganegara = (LogWargaNegara) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogWargaNegara c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return logwarganegara;
	}

	public void save(LogWargaNegara logWargaNegara) {
		this.getSessionFactory().getCurrentSession().save(logWargaNegara);
		
	}

	public void delete(LogWargaNegara logWargaNegara) {
		this.getSessionFactory().getCurrentSession().delete(logWargaNegara);
		
	}

}

package id.its.pbkk.support.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.support.dao.LogKkDao;
import id.its.pbkk.support.domain.LogKk;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("logKkDao")
public class HibLogKkDao implements LogKkDao {
private SessionFactory sessionFactory;
	
	public HibLogKkDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Transactional(readOnly=true)
	public List<LogKk> list() {
		List<LogKk> logkk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogKk")
				.list();
		return logkk;
	}

	@Transactional(readOnly=true)
	public LogKk findById(int id) {
		LogKk logkk = (LogKk) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from LogKk c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		return logkk;
	}

	public void save(LogKk logKk) {
		this.getSessionFactory().getCurrentSession().save(logKk);
		
	}

	public void delete(LogKk logKk) {
		this.getSessionFactory().getCurrentSession().delete(logKk);
		
	}
}

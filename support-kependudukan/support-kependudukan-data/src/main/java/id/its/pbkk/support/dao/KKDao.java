package id.its.pbkk.support.dao;

import id.its.pbkk.support.domain.KK;

import java.util.List;

public interface KKDao {
	public List<KK> list();
	public KK findByNomor_kk(String nomor_kk);
	public void save(KK kk);
	public void delete(KK kk);
}

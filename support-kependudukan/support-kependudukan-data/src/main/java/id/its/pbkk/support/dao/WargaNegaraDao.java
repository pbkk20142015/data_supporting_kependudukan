package id.its.pbkk.support.dao;

import id.its.pbkk.support.domain.WargaNegara;

import java.util.List;

public interface WargaNegaraDao {
	public List<WargaNegara> list();
	public WargaNegara findById(String id);
	public void save(WargaNegara warganegara);
	public void delete(WargaNegara warganegara);
	public List<WargaNegara> findByNik(String nik);

}

package id.its.pbkk.support.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.support.dao.KTPDao;
import id.its.pbkk.support.domain.KTP;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("kTPDao")
public class HibKTPDao implements KTPDao {
	
	private SessionFactory sessionFactory;
	
	public HibKTPDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Transactional(readOnly=true)
	public List<KTP> list() {
		List<KTP> ktp = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from KTP")
				.list();
		return ktp;
	}

	@Transactional(readOnly=true)
	public KTP findById(String id) {
		KTP ktp = (KTP) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from KTP c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		return ktp;
	}

	public void save(KTP kTP) {
		this.getSessionFactory().getCurrentSession().save(kTP);
		
	}

	public void delete(KTP kTP) {
		this.getSessionFactory().getCurrentSession().delete(kTP);
		
	}

}

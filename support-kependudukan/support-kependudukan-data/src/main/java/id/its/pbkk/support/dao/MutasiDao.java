package id.its.pbkk.support.dao;


import id.its.pbkk.support.domain.Mutasi;

import java.util.List;

public interface MutasiDao {
	public List<Mutasi> list();
	public Mutasi findById(String id);
	public void save(Mutasi mutasi);
	public void delete(Mutasi mutasi);

}

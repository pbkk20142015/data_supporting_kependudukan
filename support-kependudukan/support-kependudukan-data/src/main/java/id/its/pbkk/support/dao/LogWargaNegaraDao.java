package id.its.pbkk.support.dao;

import id.its.pbkk.support.domain.LogWargaNegara;

import java.util.List;

public interface LogWargaNegaraDao {
	public List<LogWargaNegara> list();
	public LogWargaNegara findById(int id);
	public void save(LogWargaNegara warganegara);
	public void delete(LogWargaNegara warganegara);
	
}

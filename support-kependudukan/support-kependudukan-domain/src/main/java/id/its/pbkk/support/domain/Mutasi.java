package id.its.pbkk.support.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name="mutasi")

public class Mutasi {
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="id")
	private UUID id;

	@Column(name="tanggal_keluar")
	private DateTime tanggal_keluar;

	@Column(name="tanggal_masuk")
	private DateTime tanggal_masuk;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public DateTime getTanggal_keluar() {
		return tanggal_keluar;
	}

	public void setTanggal_keluar(DateTime tanggal_keluar) {
		this.tanggal_keluar = tanggal_keluar;
	}

	public DateTime getTanggal_masuk() {
		return tanggal_masuk;
	}

	public void setTanggal_masuk(DateTime tanggal_masuk) {
		this.tanggal_masuk = tanggal_masuk;
	}

}

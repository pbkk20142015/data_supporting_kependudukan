package id.its.pbkk.support.domain;


import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name="ktp")
public class KTP {
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="id")
	private UUID id;

	@Column(name="alamat")
	private String alamat;

	@Column(name="berlaku_hingga")
	private DateTime berlaku_hingga;

	@Column(name="foto")
	private byte[] foto;

	@Column(name="status_berlaku")
	private String status_berlaku;

	public KTP() { } 

	public KTP(String alamat, DateTime berlaku_hingga, byte[] foto, String status_berlaku) {
		this.setAlamat(alamat);
		this.setBerlaku_hingga(berlaku_hingga);
		this.setFoto(foto);
		this.setStatus_berlaku(status_berlaku);
	}

	public DateTime getBerlaku_hingga() {
		return berlaku_hingga;
	}

	public void setBerlaku_hingga(DateTime berlaku_hingga) {
		this.berlaku_hingga = berlaku_hingga;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getStatus_berlaku() {
		return status_berlaku;
	}

	public void setStatus_berlaku(String status_berlaku) {
		this.status_berlaku = status_berlaku;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}



}

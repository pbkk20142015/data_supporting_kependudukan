package id.its.pbkk.support.domain;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;


@Entity
@Table(name="warganegara")
public class WargaNegara {
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="id")
	private UUID id;
	
	@Column(name="nik")
	private String nik;
	
	@Column(name="nama")
	private String nama;
	
	@Column(name="tanggal_lahir")
	private DateTime tanggal_lahir;
	
	@Column(name="jenis_kelamin")
	private String jenis_kelamin;
	
	@Column(name="golongan_darah")
	private String golongan_darah;
	
	@Column(name="status_kawin")
	private String status_kawin;
	
	@Column(name="status_hidup")
	private String status_hidup;
	
	@Column(name="status_hubungan_dalam_keluarga")
	private String status_hubungan_dalam_keluarga;
	
	@Column(name="nama_ayah")
	private String nama_ayah;
	
	@Column(name="nama_ibu")
	private String nama_ibu;
	
	public WargaNegara() { } 
	
	public WargaNegara(String nik, String nama, Date tanggal_lahir, String jenis_kelamin, String golongan_darah, 
			String status_kawin, String status_hidup, String status_hubungan_dalam_keluarga, String nama_ayah, 
			String nama_ibu) {
		this.setNik(nik);
		this.setTanggal_lahir(tanggal_lahir);
		this.setJenis_kelamin(jenis_kelamin);
		this.setGolongan_darah(golongan_darah);
		this.setStatus_kawin(status_kawin);
		this.setStatus_hidup(status_hidup);
		this.setStatus_hubungan_dalam_keluarga(status_hubungan_dalam_keluarga);
		this.setNama_ayah(nama_ayah);
		this.setNama_ibu(nama_ibu);
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public DateTime getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(DateTime tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}

	public String getJenis_kelamin() {
		return jenis_kelamin;
	}

	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}

	public String getGolongan_darah() {
		return golongan_darah;
	}

	public void setGolongan_darah(String golongan_darah) {
		this.golongan_darah = golongan_darah;
	}

	public String getStatus_kawin() {
		return status_kawin;
	}

	public void setStatus_kawin(String status_kawin) {
		this.status_kawin = status_kawin;
	}

	public String getStatus_hidup() {
		return status_hidup;
	}

	public void setStatus_hidup(String status_hidup) {
		this.status_hidup = status_hidup;
	}

	public String getStatus_hubungan_dalam_keluarga() {
		return status_hubungan_dalam_keluarga;
	}

	public void setStatus_hubungan_dalam_keluarga(
			String status_hubungan_dalam_keluarga) {
		this.status_hubungan_dalam_keluarga = status_hubungan_dalam_keluarga;
	}

	public String getNama_ayah() {
		return nama_ayah;
	}

	public void setNama_ayah(String nama_ayah) {
		this.nama_ayah = nama_ayah;
	}

	public String getNama_ibu() {
		return nama_ibu;
	}

	public void setNama_ibu(String nama_ibu) {
		this.nama_ibu = nama_ibu;
	}

	public String getNik() {
		return nik;
	}

	private void setTanggal_lahir(Date tanggal_lahir2) {
		// TODO Auto-generated method stub
		
	}

	private void setNik(String nik2) {
		// TODO Auto-generated method stub
		
	}
}

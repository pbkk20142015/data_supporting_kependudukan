package id.its.pbkk.support.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="LogWargaNegara")
public class LogWargaNegara {
	@Column(name="id")
	private int id;

	@Column(name="nama")
	private String nama;
	
	@Column(name="jenis_kelamin")
	private String jenis_kelamin;
	
	@Column(name="golongan_darah")
	private String golongan_darah;
	
	@Column(name="status_kawin")
	private String status_kawin;

	@Column(name="status_hidup")
	private int status_hidup;
	
	@Column(name="status_hubungan")
	private int status_hubungan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJenis_kelamin() {
		return jenis_kelamin;
	}

	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}

	public String getGolongan_darah() {
		return golongan_darah;
	}

	public void setGolongan_darah(String golongan_darah) {
		this.golongan_darah = golongan_darah;
	}

	public String getStatus_kawin() {
		return status_kawin;
	}

	public void setStatus_kawin(String status_kawin) {
		this.status_kawin = status_kawin;
	}

	public int getStatus_hidup() {
		return status_hidup;
	}

	public void setStatus_hidup(int status_hidup) {
		this.status_hidup = status_hidup;
	}

	public int getStatus_hubungan() {
		return status_hubungan;
	}

	public void setStatus_hubungan(int status_hubungan) {
		this.status_hubungan = status_hubungan;
	}
}

package id.its.pbkk.support.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="LogKtp")
public class LogKtp {
	@Column(name="id")
	private int id;

	@Column(name="alamat")
	private String alamat;
	
	@Column(name="berlaku_hingga")
	private DateTime berlaku_hingga;
	
	@Column(name="foto")
	private byte[] foto;
	
	@Column(name="status_berlaku")
	private String status_berlaku;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public DateTime getBerlaku_hingga() {
		return berlaku_hingga;
	}

	public void setBerlaku_hingga(DateTime berlaku_hingga) {
		this.berlaku_hingga = berlaku_hingga;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getStatus_berlaku() {
		return status_berlaku;
	}

	public void setStatus_berlaku(String status_berlaku) {
		this.status_berlaku = status_berlaku;
	}

}

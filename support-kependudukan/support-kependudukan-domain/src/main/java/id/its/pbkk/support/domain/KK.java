package id.its.pbkk.support.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="kk")
public class KK {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="nomor_kk")
	private UUID nomor_kk;
	
	@Column(name="alamatkk")
	private String alamatkk;
	
	public KK() { } 
	
	public KK(String alamatkk) {
		this.setAlamatkk(alamatkk);
	}

	public UUID getNomor_kk() {
		return nomor_kk;
	}

	public void setNomor_kk(UUID nomor_kk) {
		this.nomor_kk = nomor_kk;
	}

	public String getAlamatkk() {
		return alamatkk;
	}

	public void setAlamatkk(String alamatkk) {
		this.alamatkk = alamatkk;
	}
	
}

package id.its.pbkk.support.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="LogKk")
public class LogKk {
	
	@Column(name="id")
	private int id;

	@Column(name="alamat")
	private String alamat;

	public LogKk() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	} 
}

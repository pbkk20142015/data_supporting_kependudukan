/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     6/3/2015 12:23:14 AM                         */
/*==============================================================*/


drop table if exists AGAMA;

drop table if exists KK;

drop table if exists KTP;

drop table if exists LEVEL_WILAYAH;

drop table if exists MUTASI_KELUAR;

drop table if exists MUTASI_MASUK;

drop table if exists PEKERJAAN;

drop table if exists RIWAYAT_KK;

drop table if exists RIWAYAT_KTP;

drop table if exists RIWAYAT_WARGA_NEGARA;

drop table if exists WARGA_NEGARA;

drop table if exists WILAYAH;

/*==============================================================*/
/* Table: AGAMA                                                 */
/*==============================================================*/
create table AGAMA
(
   ID_AGAMA             char(2) not null,
   NAMA_AGAMA           varchar(20) not null,
   primary key (ID_AGAMA)
);

/*==============================================================*/
/* Table: KK                                                    */
/*==============================================================*/
create table KK
(
   NOMOR_KK             varchar (36) not null,
   ALAMAT_KK            varchar(500) not null,
   TANGGAL_TERBIT_KK    date not null,
   TEMPAT_TERBIT_KK     varchar (50) not null,
   primary key (NOMOR_KK)
);

/*==============================================================*/
/* Table: KTP                                                   */
/*==============================================================*/
create table KTP
(
   NOMOR_REGISTRASI     varchar (36) not null,
   UNIQUE_ID_WN         varchar (36) not null,
   ID_MUTASI_MASUK      varchar (36),
   MUT_ID_MUTASI_MASUK  varchar (36),
   ALAMAT               varchar(500) not null,
   BERLAKU_HINGGA       date not null,
   FOTO                 blob,
   STATUS_BERLAKU       smallint not null,
   TANGGAL_TERBIT	date,
   primary key (NOMOR_REGISTRASI)
);

/*==============================================================*/
/* Table: LEVEL_WILAYAH                                         */
/*==============================================================*/
create table LEVEL_WILAYAH
(
   ID_LEVEL_WILAYAH     int not null,
   NAMA_LEVEL_WILAYAH   varchar(50) not null,
   primary key (ID_LEVEL_WILAYAH)
);

/*==============================================================*/
/* Table: MUTASI_KELUAR                                         */
/*==============================================================*/
create table MUTASI_KELUAR
(
   ID_MUTASI_MASUK      varchar (36) not null,
   NOMOR_REGISTRASI     varchar (36) not null,
   TANGGAL_KELUAR       date not null,
   TUJUAN               varchar(30) not null,
   primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Table: MUTASI_MASUK                                          */
/*==============================================================*/
create table MUTASI_MASUK
(
   ID_MUTASI_MASUK      varchar (36) not null,
   NOMOR_REGISTRASI     varchar (36) not null,
   TANGGAL_MASUK        date not null,
   ASAL                 varchar(30) not null,
   primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Table: PEKERJAAN                                             */
/*==============================================================*/
create table PEKERJAAN
(
   ID_PEKERJAAN         char(3) not null,
   NAMA_PEKERJAAN       varchar(20) not null,
   primary key (ID_PEKERJAAN)
);

/*==============================================================*/
/* Table: RIWAYAT_KK                                            */
/*==============================================================*/
create table RIWAYAT_KK
(
   ID_RIWAYAT_KK        int not null,
   NOMOR_KK             varchar (36) not null,
   ALAMAT_RIWAYAT_KK    varchar(500) not null,
   TANGGAL_TERBIT_KK    date not null,
   TEMPAT_TERBIT_KK     date not null,
   primary key (ID_RIWAYAT_KK)
);

/*==============================================================*/
/* Table: RIWAYAT_KTP                                           */
/*==============================================================*/
create table RIWAYAT_KTP
(
   ID_RIWAYAT_KTP       int not null,
   NOMOR_REGISTRASI     varchar (36) not null,
   ALAMAT_RIWAYAT       varchar(500) not null,
   BERLAKU_HINGGA_RIWAYAT date not null,
   FOTO_RIWAYAT         blob,
   STATUS_BERLAKU_RIWAYAT smallint not null,
   primary key (ID_RIWAYAT_KTP)
);

/*==============================================================*/
/* Table: RIWAYAT_WARGA_NEGARA                                  */
/*==============================================================*/
create table RIWAYAT_WARGA_NEGARA
(
   ID_RIWAYAT_WARGA_NEGARA int not null,
   UNIQUE_ID_WN         varchar (36) not null,
   NAMA_RIWAYAT         varchar(250) not null,
   JENIS_KELAMIN_RIWAYAT char(1) not null,
   GOLONGAN_DARAH_RIWAYAT char(2) not null,
   STATUS_KAWIN_RIWAYAT char(1) not null,
   STATUS_HIDUP_RIWAYAT smallint not null,
   STATUS_HUBUNGAN_DALAM_KELUARGA__RIWAYAT int not null,
   primary key (ID_RIWAYAT_WARGA_NEGARA)
);

/*==============================================================*/
/* Table: WARGA_NEGARA                                          */
/*==============================================================*/
create table WARGA_NEGARA
(
   UNIQUE_ID_WN         varchar (36) not null,
   NOMOR_KK             varchar (36),
   ID_PEKERJAAN         char(3) not null,
   ID_AGAMA             char(2) not null,
   NIK                  char(16) not null,
   NAMA                 varchar(250) not null,
   TANGGAL_LAHIR        date not null,
   JENIS_KELAMIN        char(1) not null,
   GOLONGAN_DARAH       char(2) not null,
   STATUS_KAWIN         char(1),
   STATUS_HIDUP         smallint not null,
   SHDK2                int not null,
   NAMA_AYAH            varchar(250) not null,
   NAMA_IBU             varchar(250) not null,
   primary key (UNIQUE_ID_WN)
);

/*==============================================================*/
/* Table: WILAYAH                                               */
/*==============================================================*/
create table WILAYAH
(
   ID_WILAYAH           int not null,
   WIL_ID_WILAYAH       int,
   ID_LEVEL_WILAYAH     int not null,
   NAMA_WILAYAH         varchar(50) not null,
   primary key (ID_WILAYAH)
);

alter table KTP add constraint FK_RELATIONSHIP_16 foreign key (MUT_ID_MUTASI_MASUK)
      references MUTASI_KELUAR (ID_MUTASI_MASUK) on delete restrict on update restrict;

alter table KTP add constraint FK_RELATIONSHIP_19 foreign key (ID_MUTASI_MASUK)
      references MUTASI_MASUK (ID_MUTASI_MASUK) on delete restrict on update restrict;

alter table KTP add constraint FK_RELATIONSHIP_2 foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN) on delete restrict on update restrict;

alter table MUTASI_KELUAR add constraint FK_RELATIONSHIP_14 foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI) on delete restrict on update restrict;

alter table MUTASI_MASUK add constraint FK_RELATIONSHIP_17 foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI) on delete restrict on update restrict;

alter table RIWAYAT_KK add constraint FK_RELATIONSHIP_18 foreign key (NOMOR_KK)
      references KK (NOMOR_KK) on delete restrict on update restrict;

alter table RIWAYAT_KTP add constraint FK_RELATIONSHIP_11 foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI) on delete restrict on update restrict;

alter table RIWAYAT_WARGA_NEGARA add constraint FK_RELATIONSHIP_15 foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN) on delete restrict on update restrict;

alter table WARGA_NEGARA add constraint FK_RELATIONSHIP_1 foreign key (NOMOR_KK)
      references KK (NOMOR_KK) on delete restrict on update restrict;

alter table WARGA_NEGARA add constraint FK_RELATIONSHIP_20 foreign key (ID_AGAMA)
      references AGAMA (ID_AGAMA) on delete restrict on update restrict;

alter table WARGA_NEGARA add constraint FK_RELATIONSHIP_21 foreign key (ID_PEKERJAAN)
      references PEKERJAAN (ID_PEKERJAAN) on delete restrict on update restrict;

alter table WILAYAH add constraint FK_INDUK foreign key (WIL_ID_WILAYAH)
      references WILAYAH (ID_WILAYAH) on delete restrict on update restrict;

alter table WILAYAH add constraint FK_RELATIONSHIP_13 foreign key (ID_LEVEL_WILAYAH)
      references LEVEL_WILAYAH (ID_LEVEL_WILAYAH) on delete restrict on update restrict;


/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     02/06/2015 21:49:43                          */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_MUTASI_M') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_MUTASI_M
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_WARGA_NE') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_WARGA_NE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_MUTASI_K') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_MUTASI_K
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_LOG_KK_RELATIONS_KK') then
    alter table LOG_KK
       delete foreign key FK_LOG_KK_RELATIONS_KK
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_LOG_KTP_RELATIONS_KTP') then
    alter table LOG_KTP
       delete foreign key FK_LOG_KTP_RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_LOG_WARG_RELATIONS_WARGA_NE') then
    alter table LOG_WARGA_NEGARA
       delete foreign key FK_LOG_WARG_RELATIONS_WARGA_NE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_K_RELATIONS_WILAYAH') then
    alter table MUTASI_KELUAR
       delete foreign key FK_MUTASI_K_RELATIONS_WILAYAH
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_K_RELATIONS_KTP') then
    alter table MUTASI_KELUAR
       delete foreign key FK_MUTASI_K_RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_M_RELATIONS_KTP') then
    alter table MUTASI_MASUK
       delete foreign key FK_MUTASI_M_RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_M_RELATIONS_WILAYAH') then
    alter table MUTASI_MASUK
       delete foreign key FK_MUTASI_M_RELATIONS_WILAYAH
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_KK') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_KK
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_PENDIDIK') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_PENDIDIK
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_AGAMA') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_AGAMA
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_PEKERJAA') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_PEKERJAA
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WILAYAH_INDUK_WILAYAH') then
    alter table WILAYAH
       delete foreign key FK_WILAYAH_INDUK_WILAYAH
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WILAYAH_RELATIONS_LEVEL_WI') then
    alter table WILAYAH
       delete foreign key FK_WILAYAH_RELATIONS_LEVEL_WI
end if;

drop index if exists AGAMA.AGAMA_PK;

drop table if exists AGAMA;

drop index if exists KK.KK_PK;

drop table if exists KK;

drop index if exists KTP.RELATIONSHIP_22_FK;

drop index if exists KTP.RELATIONSHIP_12_FK;

drop index if exists KTP.RELATIONSHIP_2_FK;

drop index if exists KTP.KTP_PK;

drop table if exists KTP;

drop index if exists LEVEL_WILAYAH.LEVEL_WILAYAH_PK;

drop table if exists LEVEL_WILAYAH;

drop index if exists LOG_KK.RELATIONSHIP_18_FK;

drop index if exists LOG_KK.LOG_KK_PK;

drop table if exists LOG_KK;

drop index if exists LOG_KTP.RELATIONSHIP_11_FK;

drop index if exists LOG_KTP.LOG_KTP_PK;

drop table if exists LOG_KTP;

drop index if exists LOG_WARGA_NEGARA.RELATIONSHIP_15_FK;

drop index if exists LOG_WARGA_NEGARA.LOG_WARGA_NEGARA_PK;

drop table if exists LOG_WARGA_NEGARA;

drop index if exists MUTASI_KELUAR.RELATIONSHIP_19_FK;

drop index if exists MUTASI_KELUAR.RELATIONSHIP_17_FK;

drop index if exists MUTASI_KELUAR.MUTASI_KELUAR_PK;

drop table if exists MUTASI_KELUAR;

drop index if exists MUTASI_MASUK.RELATIONSHIP_16_FK;

drop index if exists MUTASI_MASUK.RELATIONSHIP_14_FK;

drop index if exists MUTASI_MASUK.MUTASI_MASUK_PK;

drop table if exists MUTASI_MASUK;

drop index if exists PEKERJAAN.PEKERJAAN_PK;

drop table if exists PEKERJAAN;

drop index if exists PENDIDIKAN.PENDIDIKAN_PK;

drop table if exists PENDIDIKAN;

drop index if exists WARGA_NEGARA.RELATIONSHIP_10_FK;

drop index if exists WARGA_NEGARA.RELATIONSHIP_1_FK;

drop index if exists WARGA_NEGARA.RELATIONSHIP_21_FK;

drop index if exists WARGA_NEGARA.RELATIONSHIP_20_FK;

drop index if exists WARGA_NEGARA.WARGA_NEGARA_PK;

drop table if exists WARGA_NEGARA;

drop index if exists WILAYAH.INDUK_FK;

drop index if exists WILAYAH.RELATIONSHIP_13_FK;

drop index if exists WILAYAH.WILAYAH_PK;

drop table if exists WILAYAH;

/*==============================================================*/
/* Table: AGAMA                                                 */
/*==============================================================*/
create table AGAMA 
(
   ID_AGAMA             char(2)                        not null,
   NAMA_AGAMA           varchar(20)                    not null,
   constraint PK_AGAMA primary key (ID_AGAMA)
);

/*==============================================================*/
/* Index: AGAMA_PK                                              */
/*==============================================================*/
create unique index AGAMA_PK on AGAMA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Table: KK                                                    */
/*==============================================================*/
create table KK 
(
   NOMOR_KK             UUID                           not null,
   ALAMAT_KK            varchar(500)                   not null,
   constraint PK_KK primary key (NOMOR_KK)
);

/*==============================================================*/
/* Index: KK_PK                                                 */
/*==============================================================*/
create unique index KK_PK on KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: KTP                                                   */
/*==============================================================*/
create table KTP 
(
   NOMOR_REGISTRASI     UUID                           not null,
   UNIQUE_ID_WN         UUID                           not null,
   ID_MUTASI_MASUK      UUID                           null,
   ID_MUTASI_KELUAR     UUID                           null,
   ALAMAT               varchar(500)                   not null,
   BERLAKU_HINGGA       date                           not null,
   FOTO                 binary(64000)                  null,
   STATUS_BERLAKU       smallint                       not null,
   constraint PK_KTP primary key (NOMOR_REGISTRASI)
);

/*==============================================================*/
/* Index: KTP_PK                                                */
/*==============================================================*/
create unique index KTP_PK on KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on KTP (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_12_FK on KTP (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_22_FK on KTP (
ID_MUTASI_KELUAR ASC
);

/*==============================================================*/
/* Table: LEVEL_WILAYAH                                         */
/*==============================================================*/
create table LEVEL_WILAYAH 
(
   ID_LEVEL_WILAYAH     integer                        not null,
   NAMA_LEVEL_WILAYAH   varchar(50)                    not null,
   constraint PK_LEVEL_WILAYAH primary key (ID_LEVEL_WILAYAH)
);

/*==============================================================*/
/* Index: LEVEL_WILAYAH_PK                                      */
/*==============================================================*/
create unique index LEVEL_WILAYAH_PK on LEVEL_WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Table: LOG_KK                                                */
/*==============================================================*/
create table LOG_KK 
(
   ID_LOG_KK            integer                        not null,
   NOMOR_KK             UUID                           not null,
   ALAMAT_LOG_KK        varchar(500)                   not null,
   constraint PK_LOG_KK primary key (ID_LOG_KK)
);

/*==============================================================*/
/* Index: LOG_KK_PK                                             */
/*==============================================================*/
create unique index LOG_KK_PK on LOG_KK (
ID_LOG_KK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_18_FK on LOG_KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: LOG_KTP                                               */
/*==============================================================*/
create table LOG_KTP 
(
   ID_LOG_KTP           integer                        not null,
   NOMOR_REGISTRASI     UUID                           not null,
   ALAMAT_LOG           varchar(500)                   not null,
   BERLAKU_HINGGA_LOG   date                           not null,
   FOTO_LOG             binary(64000)                  null,
   STATUS_BERLAKU_LOG   smallint                       not null,
   constraint PK_LOG_KTP primary key (ID_LOG_KTP)
);

/*==============================================================*/
/* Index: LOG_KTP_PK                                            */
/*==============================================================*/
create unique index LOG_KTP_PK on LOG_KTP (
ID_LOG_KTP ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_11_FK on LOG_KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: LOG_WARGA_NEGARA                                      */
/*==============================================================*/
create table LOG_WARGA_NEGARA 
(
   ID_LOG_WARGA_NEGARA  integer                        not null,
   UNIQUE_ID_WN         UUID                           not null,
   NAMA_LOG             varchar(250)                   not null,
   JENIS_KELAMIN_LOG    char(1)                        not null,
   GOLONGAN_DARAH_LOG   char(2)                        not null,
   STATUS_KAWIN_LOG     char(1)                        not null,
   STATUS_HIDUP_LOG     smallint                       not null,
   STATUS_HUBUNGAN_DALAM_KELUARGA__LOG integer                        not null,
   constraint PK_LOG_WARGA_NEGARA primary key (ID_LOG_WARGA_NEGARA)
);

/*==============================================================*/
/* Index: LOG_WARGA_NEGARA_PK                                   */
/*==============================================================*/
create unique index LOG_WARGA_NEGARA_PK on LOG_WARGA_NEGARA (
ID_LOG_WARGA_NEGARA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_15_FK on LOG_WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Table: MUTASI_KELUAR                                         */
/*==============================================================*/
create table MUTASI_KELUAR 
(
   ID_MUTASI_KELUAR     UUID                           not null,
   ID_WILAYAH           integer                        not null,
   NOMOR_REGISTRASI     UUID                           null,
   TANGGALKELUAR        date                           not null,
   constraint PK_MUTASI_KELUAR primary key (ID_MUTASI_KELUAR)
);

/*==============================================================*/
/* Index: MUTASI_KELUAR_PK                                      */
/*==============================================================*/
create unique index MUTASI_KELUAR_PK on MUTASI_KELUAR (
ID_MUTASI_KELUAR ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_17_FK on MUTASI_KELUAR (
ID_WILAYAH ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_19_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_19_FK on MUTASI_KELUAR (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: MUTASI_MASUK                                          */
/*==============================================================*/
create table MUTASI_MASUK 
(
   ID_MUTASI_MASUK      UUID                           not null,
   NOMOR_REGISTRASI     UUID                           not null,
   ID_WILAYAH           integer                        not null,
   TANGGAL_MASUK        date                           null,
   constraint PK_MUTASI_MASUK primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Index: MUTASI_MASUK_PK                                       */
/*==============================================================*/
create unique index MUTASI_MASUK_PK on MUTASI_MASUK (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_14_FK on MUTASI_MASUK (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_16_FK on MUTASI_MASUK (
ID_WILAYAH ASC
);

/*==============================================================*/
/* Table: PEKERJAAN                                             */
/*==============================================================*/
create table PEKERJAAN 
(
   ID_PEKERJAAN         char(3)                        not null,
   NAMA_PEKERJAAN       varchar(20)                    not null,
   constraint PK_PEKERJAAN primary key (ID_PEKERJAAN)
);

/*==============================================================*/
/* Index: PEKERJAAN_PK                                          */
/*==============================================================*/
create unique index PEKERJAAN_PK on PEKERJAAN (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Table: PENDIDIKAN                                            */
/*==============================================================*/
create table PENDIDIKAN 
(
   ID_PENDIDIKAN        char(2)                        not null,
   NAMA_PENDIDIKAN      varchar(50)                    not null,
   constraint PK_PENDIDIKAN primary key (ID_PENDIDIKAN)
);

/*==============================================================*/
/* Index: PENDIDIKAN_PK                                         */
/*==============================================================*/
create unique index PENDIDIKAN_PK on PENDIDIKAN (
ID_PENDIDIKAN ASC
);

/*==============================================================*/
/* Table: WARGA_NEGARA                                          */
/*==============================================================*/
create table WARGA_NEGARA 
(
   UNIQUE_ID_WN         UUID                           not null,
   ID_AGAMA             char(2)                        not null,
   ID_PEKERJAAN         char(3)                        not null,
   NOMOR_KK             UUID                           null,
   ID_PENDIDIKAN        char(2)                        not null,
   NIK                  char(16)                       not null,
   NAMA                 varchar(250)                   not null,
   TANGGAL_LAHIR        date                           not null,
   JENIS_KELAMIN        char(1)                        not null
   	constraint CKC_JENIS_KELAMIN_WARGA_NE check (JENIS_KELAMIN in ('L','P')),
   GOLONGAN_DARAH       char(2)                        not null
   	constraint CKC_GOLONGAN_DARAH_WARGA_NE check (GOLONGAN_DARAH in ('A','B','AB','O')),
   STATUS_KAWIN         char(1)                        null,
   STATUS_HIDUP         smallint                       not null,
   SHDK2                integer                        not null
   	constraint CKC_SHDK2_WARGA_NE check (SHDK2 in (1,2,3,4,5)),
   NAMA_AYAH            varchar(250)                   not null,
   NAMA_IBU             varchar(250)                   not null,
   constraint PK_WARGA_NEGARA primary key (UNIQUE_ID_WN)
);

/*==============================================================*/
/* Index: WARGA_NEGARA_PK                                       */
/*==============================================================*/
create unique index WARGA_NEGARA_PK on WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_20_FK on WARGA_NEGARA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_21_FK on WARGA_NEGARA (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_1_FK on WARGA_NEGARA (
NOMOR_KK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_10_FK on WARGA_NEGARA (
ID_PENDIDIKAN ASC
);

/*==============================================================*/
/* Table: WILAYAH                                               */
/*==============================================================*/
create table WILAYAH 
(
   ID_WILAYAH           integer                        not null,
   ID_LEVEL_WILAYAH     integer                        not null,
   WIL_ID_WILAYAH       integer                        null,
   NAMA_WILAYAH         varchar(50)                    not null,
   constraint PK_WILAYAH primary key (ID_WILAYAH)
);

/*==============================================================*/
/* Index: WILAYAH_PK                                            */
/*==============================================================*/
create unique index WILAYAH_PK on WILAYAH (
ID_WILAYAH ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_13_FK on WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Index: INDUK_FK                                              */
/*==============================================================*/
create index INDUK_FK on WILAYAH (
WIL_ID_WILAYAH ASC
);

alter table KTP
   add constraint FK_KTP_RELATIONS_MUTASI_M foreign key (ID_MUTASI_MASUK)
      references MUTASI_MASUK (ID_MUTASI_MASUK)
      on update restrict
      on delete restrict;

alter table KTP
   add constraint FK_KTP_RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table KTP
   add constraint FK_KTP_RELATIONS_MUTASI_K foreign key (ID_MUTASI_KELUAR)
      references MUTASI_KELUAR (ID_MUTASI_KELUAR)
      on update restrict
      on delete restrict;

alter table LOG_KK
   add constraint FK_LOG_KK_RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table LOG_KTP
   add constraint FK_LOG_KTP_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table LOG_WARGA_NEGARA
   add constraint FK_LOG_WARG_RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table MUTASI_KELUAR
   add constraint FK_MUTASI_K_RELATIONS_WILAYAH foreign key (ID_WILAYAH)
      references WILAYAH (ID_WILAYAH)
      on update restrict
      on delete restrict;

alter table MUTASI_KELUAR
   add constraint FK_MUTASI_K_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table MUTASI_MASUK
   add constraint FK_MUTASI_M_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table MUTASI_MASUK
   add constraint FK_MUTASI_M_RELATIONS_WILAYAH foreign key (ID_WILAYAH)
      references WILAYAH (ID_WILAYAH)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_PENDIDIK foreign key (ID_PENDIDIKAN)
      references PENDIDIKAN (ID_PENDIDIKAN)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_AGAMA foreign key (ID_AGAMA)
      references AGAMA (ID_AGAMA)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_PEKERJAA foreign key (ID_PEKERJAAN)
      references PEKERJAAN (ID_PEKERJAAN)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_INDUK_WILAYAH foreign key (WIL_ID_WILAYAH)
      references WILAYAH (ID_WILAYAH)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_RELATIONS_LEVEL_WI foreign key (ID_LEVEL_WILAYAH)
      references LEVEL_WILAYAH (ID_LEVEL_WILAYAH)
      on update restrict
      on delete restrict;


/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 10                       */
/* Created on:     03/06/2015 0:17:32                           */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_MUTASI_K') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_MUTASI_K
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_MUTASI_M') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_MUTASI_M
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_KTP_RELATIONS_WARGA_NE') then
    alter table KTP
       delete foreign key FK_KTP_RELATIONS_WARGA_NE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_K_RELATIONS_KTP') then
    alter table MUTASI_KELUAR
       delete foreign key FK_MUTASI_K_RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MUTASI_M_RELATIONS_KTP') then
    alter table MUTASI_MASUK
       delete foreign key FK_MUTASI_M_RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_RIWAYAT__RELATIONS_KK') then
    alter table RIWAYAT_KK
       delete foreign key FK_RIWAYAT__RELATIONS_KK
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_RIWAYAT__RELATIONS_KTP') then
    alter table RIWAYAT_KTP
       delete foreign key FK_RIWAYAT__RELATIONS_KTP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_RIWAYAT__RELATIONS_WARGA_NE') then
    alter table RIWAYAT_WARGA_NEGARA
       delete foreign key FK_RIWAYAT__RELATIONS_WARGA_NE
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_KK') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_KK
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_AGAMA') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_AGAMA
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WARGA_NE_RELATIONS_PEKERJAA') then
    alter table WARGA_NEGARA
       delete foreign key FK_WARGA_NE_RELATIONS_PEKERJAA
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WILAYAH_INDUK_WILAYAH') then
    alter table WILAYAH
       delete foreign key FK_WILAYAH_INDUK_WILAYAH
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_WILAYAH_RELATIONS_LEVEL_WI') then
    alter table WILAYAH
       delete foreign key FK_WILAYAH_RELATIONS_LEVEL_WI
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='AGAMA_PK'
     and t.table_name='AGAMA'
) then
   drop index AGAMA.AGAMA_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='AGAMA'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table AGAMA
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='KK_PK'
     and t.table_name='KK'
) then
   drop index KK.KK_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='KK'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table KK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_19_FK'
     and t.table_name='KTP'
) then
   drop index KTP.RELATIONSHIP_19_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_16_FK'
     and t.table_name='KTP'
) then
   drop index KTP.RELATIONSHIP_16_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_2_FK'
     and t.table_name='KTP'
) then
   drop index KTP.RELATIONSHIP_2_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='KTP_PK'
     and t.table_name='KTP'
) then
   drop index KTP.KTP_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='KTP'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table KTP
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='LEVEL_WILAYAH_PK'
     and t.table_name='LEVEL_WILAYAH'
) then
   drop index LEVEL_WILAYAH.LEVEL_WILAYAH_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='LEVEL_WILAYAH'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table LEVEL_WILAYAH
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_14_FK'
     and t.table_name='MUTASI_KELUAR'
) then
   drop index MUTASI_KELUAR.RELATIONSHIP_14_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='MUTASI_KELUAR_PK'
     and t.table_name='MUTASI_KELUAR'
) then
   drop index MUTASI_KELUAR.MUTASI_KELUAR_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='MUTASI_KELUAR'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table MUTASI_KELUAR
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_17_FK'
     and t.table_name='MUTASI_MASUK'
) then
   drop index MUTASI_MASUK.RELATIONSHIP_17_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='MUTASI_MASUK_PK'
     and t.table_name='MUTASI_MASUK'
) then
   drop index MUTASI_MASUK.MUTASI_MASUK_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='MUTASI_MASUK'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table MUTASI_MASUK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='PEKERJAAN_PK'
     and t.table_name='PEKERJAAN'
) then
   drop index PEKERJAAN.PEKERJAAN_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='PEKERJAAN'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table PEKERJAAN
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_18_FK'
     and t.table_name='RIWAYAT_KK'
) then
   drop index RIWAYAT_KK.RELATIONSHIP_18_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RIWAYAT_KK_PK'
     and t.table_name='RIWAYAT_KK'
) then
   drop index RIWAYAT_KK.RIWAYAT_KK_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='RIWAYAT_KK'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table RIWAYAT_KK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_11_FK'
     and t.table_name='RIWAYAT_KTP'
) then
   drop index RIWAYAT_KTP.RELATIONSHIP_11_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RIWAYAT_KTP_PK'
     and t.table_name='RIWAYAT_KTP'
) then
   drop index RIWAYAT_KTP.RIWAYAT_KTP_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='RIWAYAT_KTP'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table RIWAYAT_KTP
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_15_FK'
     and t.table_name='RIWAYAT_WARGA_NEGARA'
) then
   drop index RIWAYAT_WARGA_NEGARA.RELATIONSHIP_15_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RIWAYAT_WARGA_NEGARA_PK'
     and t.table_name='RIWAYAT_WARGA_NEGARA'
) then
   drop index RIWAYAT_WARGA_NEGARA.RIWAYAT_WARGA_NEGARA_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='RIWAYAT_WARGA_NEGARA'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table RIWAYAT_WARGA_NEGARA
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_1_FK'
     and t.table_name='WARGA_NEGARA'
) then
   drop index WARGA_NEGARA.RELATIONSHIP_1_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_21_FK'
     and t.table_name='WARGA_NEGARA'
) then
   drop index WARGA_NEGARA.RELATIONSHIP_21_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_20_FK'
     and t.table_name='WARGA_NEGARA'
) then
   drop index WARGA_NEGARA.RELATIONSHIP_20_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='WARGA_NEGARA_PK'
     and t.table_name='WARGA_NEGARA'
) then
   drop index WARGA_NEGARA.WARGA_NEGARA_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='WARGA_NEGARA'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table WARGA_NEGARA
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='INDUK_FK'
     and t.table_name='WILAYAH'
) then
   drop index WILAYAH.INDUK_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='RELATIONSHIP_13_FK'
     and t.table_name='WILAYAH'
) then
   drop index WILAYAH.RELATIONSHIP_13_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='WILAYAH_PK'
     and t.table_name='WILAYAH'
) then
   drop index WILAYAH.WILAYAH_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='WILAYAH'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table WILAYAH
end if;

/*==============================================================*/
/* Table: AGAMA                                                 */
/*==============================================================*/
create table AGAMA 
(
   ID_AGAMA             char(2)                        not null,
   NAMA_AGAMA           varchar(20)                    not null,
   constraint PK_AGAMA primary key (ID_AGAMA)
);

/*==============================================================*/
/* Index: AGAMA_PK                                              */
/*==============================================================*/
create unique index AGAMA_PK on AGAMA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Table: KK                                                    */
/*==============================================================*/
create table KK 
(
   NOMOR_KK             UUID                           not null,
   ALAMAT_KK            varchar(500)                   not null,
   TANGGAL_TERBIT_KK    date                           not null,
   TEMPAT_TERBIT_KK     date                           not null,
   constraint PK_KK primary key (NOMOR_KK)
);

/*==============================================================*/
/* Index: KK_PK                                                 */
/*==============================================================*/
create unique index KK_PK on KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: KTP                                                   */
/*==============================================================*/
create table KTP 
(
   NOMOR_REGISTRASI     UUID                           not null,
   UNIQUE_ID_WN         UUID                           not null,
   ID_MUTASI_MASUK      UUID                           null,
   MUT_ID_MUTASI_MASUK  UUID                           null,
   ALAMAT               varchar(500)                   not null,
   BERLAKU_HINGGA       date                           not null,
   FOTO                 binary(64000)                  null,
   STATUS_BERLAKU       smallint                       not null,
   constraint PK_KTP primary key (NOMOR_REGISTRASI)
);

/*==============================================================*/
/* Index: KTP_PK                                                */
/*==============================================================*/
create unique index KTP_PK on KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on KTP (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_16_FK on KTP (
MUT_ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_19_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_19_FK on KTP (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Table: LEVEL_WILAYAH                                         */
/*==============================================================*/
create table LEVEL_WILAYAH 
(
   ID_LEVEL_WILAYAH     integer                        not null,
   NAMA_LEVEL_WILAYAH   varchar(50)                    not null,
   constraint PK_LEVEL_WILAYAH primary key (ID_LEVEL_WILAYAH)
);

/*==============================================================*/
/* Index: LEVEL_WILAYAH_PK                                      */
/*==============================================================*/
create unique index LEVEL_WILAYAH_PK on LEVEL_WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Table: MUTASI_KELUAR                                         */
/*==============================================================*/
create table MUTASI_KELUAR 
(
   ID_MUTASI_MASUK      UUID                           not null,
   NOMOR_REGISTRASI     UUID                           not null,
   TANGGAL_KELUAR       date                           not null,
   TUJUAN               varchar(30)                    not null,
   constraint PK_MUTASI_KELUAR primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Index: MUTASI_KELUAR_PK                                      */
/*==============================================================*/
create unique index MUTASI_KELUAR_PK on MUTASI_KELUAR (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_14_FK on MUTASI_KELUAR (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: MUTASI_MASUK                                          */
/*==============================================================*/
create table MUTASI_MASUK 
(
   ID_MUTASI_MASUK      UUID                           not null,
   NOMOR_REGISTRASI     UUID                           not null,
   TANGGAL_MASUK        date                           not null,
   ASAL                 varchar(30)                    not null,
   constraint PK_MUTASI_MASUK primary key (ID_MUTASI_MASUK)
);

/*==============================================================*/
/* Index: MUTASI_MASUK_PK                                       */
/*==============================================================*/
create unique index MUTASI_MASUK_PK on MUTASI_MASUK (
ID_MUTASI_MASUK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_17_FK on MUTASI_MASUK (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: PEKERJAAN                                             */
/*==============================================================*/
create table PEKERJAAN 
(
   ID_PEKERJAAN         char(3)                        not null,
   NAMA_PEKERJAAN       varchar(20)                    not null,
   constraint PK_PEKERJAAN primary key (ID_PEKERJAAN)
);

/*==============================================================*/
/* Index: PEKERJAAN_PK                                          */
/*==============================================================*/
create unique index PEKERJAAN_PK on PEKERJAAN (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Table: RIWAYAT_KK                                            */
/*==============================================================*/
create table RIWAYAT_KK 
(
   ID_RIWAYAT_KK        integer                        not null,
   NOMOR_KK             UUID                           not null,
   ALAMAT_RIWAYAT_KK    varchar(500)                   not null,
   TANGGAL_TERBIT_KK    date                           not null,
   TEMPAT_TERBIT_KK     date                           not null,
   constraint PK_RIWAYAT_KK primary key (ID_RIWAYAT_KK)
);

/*==============================================================*/
/* Index: RIWAYAT_KK_PK                                         */
/*==============================================================*/
create unique index RIWAYAT_KK_PK on RIWAYAT_KK (
ID_RIWAYAT_KK ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_18_FK on RIWAYAT_KK (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: RIWAYAT_KTP                                           */
/*==============================================================*/
create table RIWAYAT_KTP 
(
   ID_RIWAYAT_KTP       integer                        not null,
   NOMOR_REGISTRASI     UUID                           not null,
   ALAMAT_RIWAYAT       varchar(500)                   not null,
   BERLAKU_HINGGA_RIWAYAT date                           not null,
   FOTO_RIWAYAT         binary(64000)                  null,
   STATUS_BERLAKU_RIWAYAT smallint                       not null,
   constraint PK_RIWAYAT_KTP primary key (ID_RIWAYAT_KTP)
);

/*==============================================================*/
/* Index: RIWAYAT_KTP_PK                                        */
/*==============================================================*/
create unique index RIWAYAT_KTP_PK on RIWAYAT_KTP (
ID_RIWAYAT_KTP ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_11_FK on RIWAYAT_KTP (
NOMOR_REGISTRASI ASC
);

/*==============================================================*/
/* Table: RIWAYAT_WARGA_NEGARA                                  */
/*==============================================================*/
create table RIWAYAT_WARGA_NEGARA 
(
   ID_RIWAYAT_WARGA_NEGARA integer                        not null,
   UNIQUE_ID_WN         UUID                           not null,
   NAMA_RIWAYAT         varchar(250)                   not null,
   JENIS_KELAMIN_RIWAYAT char(1)                        not null,
   GOLONGAN_DARAH_RIWAYAT char(2)                        not null,
   STATUS_KAWIN_RIWAYAT char(1)                        not null,
   STATUS_HIDUP_RIWAYAT smallint                       not null,
   STATUS_HUBUNGAN_DALAM_KELUARGA__RIWAYAT integer                        not null,
   constraint PK_RIWAYAT_WARGA_NEGARA primary key (ID_RIWAYAT_WARGA_NEGARA)
);

/*==============================================================*/
/* Index: RIWAYAT_WARGA_NEGARA_PK                               */
/*==============================================================*/
create unique index RIWAYAT_WARGA_NEGARA_PK on RIWAYAT_WARGA_NEGARA (
ID_RIWAYAT_WARGA_NEGARA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_15_FK on RIWAYAT_WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Table: WARGA_NEGARA                                          */
/*==============================================================*/
create table WARGA_NEGARA 
(
   UNIQUE_ID_WN         UUID                           not null,
   NOMOR_KK             UUID                           null,
   ID_PEKERJAAN         char(3)                        not null,
   ID_AGAMA             char(2)                        not null,
   NIK                  char(16)                       not null,
   NAMA                 varchar(250)                   not null,
   TANGGAL_LAHIR        date                           not null,
   JENIS_KELAMIN        char(1)                        not null
      constraint CKC_JENIS_KELAMIN_WARGA_NE check (JENIS_KELAMIN in ('L','P')),
   GOLONGAN_DARAH       char(2)                        not null
      constraint CKC_GOLONGAN_DARAH_WARGA_NE check (GOLONGAN_DARAH in ('A','B','AB','O')),
   STATUS_KAWIN         char(1)                        null,
   STATUS_HIDUP         smallint                       not null,
   SHDK2                integer                        not null
      constraint CKC_SHDK2_WARGA_NE check (SHDK2 in (1,2,3,4,5)),
   NAMA_AYAH            varchar(250)                   not null,
   NAMA_IBU             varchar(250)                   not null,
   constraint PK_WARGA_NEGARA primary key (UNIQUE_ID_WN)
);

/*==============================================================*/
/* Index: WARGA_NEGARA_PK                                       */
/*==============================================================*/
create unique index WARGA_NEGARA_PK on WARGA_NEGARA (
UNIQUE_ID_WN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_20_FK on WARGA_NEGARA (
ID_AGAMA ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_21_FK on WARGA_NEGARA (
ID_PEKERJAAN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_1_FK on WARGA_NEGARA (
NOMOR_KK ASC
);

/*==============================================================*/
/* Table: WILAYAH                                               */
/*==============================================================*/
create table WILAYAH 
(
   ID_WILAYAH           integer                        not null,
   WIL_ID_WILAYAH       integer                        null,
   ID_LEVEL_WILAYAH     integer                        not null,
   NAMA_WILAYAH         varchar(50)                    not null,
   constraint PK_WILAYAH primary key (ID_WILAYAH)
);

/*==============================================================*/
/* Index: WILAYAH_PK                                            */
/*==============================================================*/
create unique index WILAYAH_PK on WILAYAH (
ID_WILAYAH ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_13_FK on WILAYAH (
ID_LEVEL_WILAYAH ASC
);

/*==============================================================*/
/* Index: INDUK_FK                                              */
/*==============================================================*/
create index INDUK_FK on WILAYAH (
WIL_ID_WILAYAH ASC
);

alter table KTP
   add constraint FK_KTP_RELATIONS_MUTASI_K foreign key (MUT_ID_MUTASI_MASUK)
      references MUTASI_KELUAR (ID_MUTASI_MASUK)
      on update restrict
      on delete restrict;

alter table KTP
   add constraint FK_KTP_RELATIONS_MUTASI_M foreign key (ID_MUTASI_MASUK)
      references MUTASI_MASUK (ID_MUTASI_MASUK)
      on update restrict
      on delete restrict;

alter table KTP
   add constraint FK_KTP_RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table MUTASI_KELUAR
   add constraint FK_MUTASI_K_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table MUTASI_MASUK
   add constraint FK_MUTASI_M_RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table RIWAYAT_KK
   add constraint FK_RIWAYAT__RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table RIWAYAT_KTP
   add constraint FK_RIWAYAT__RELATIONS_KTP foreign key (NOMOR_REGISTRASI)
      references KTP (NOMOR_REGISTRASI)
      on update restrict
      on delete restrict;

alter table RIWAYAT_WARGA_NEGARA
   add constraint FK_RIWAYAT__RELATIONS_WARGA_NE foreign key (UNIQUE_ID_WN)
      references WARGA_NEGARA (UNIQUE_ID_WN)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_KK foreign key (NOMOR_KK)
      references KK (NOMOR_KK)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_AGAMA foreign key (ID_AGAMA)
      references AGAMA (ID_AGAMA)
      on update restrict
      on delete restrict;

alter table WARGA_NEGARA
   add constraint FK_WARGA_NE_RELATIONS_PEKERJAA foreign key (ID_PEKERJAAN)
      references PEKERJAAN (ID_PEKERJAAN)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_INDUK_WILAYAH foreign key (WIL_ID_WILAYAH)
      references WILAYAH (ID_WILAYAH)
      on update restrict
      on delete restrict;

alter table WILAYAH
   add constraint FK_WILAYAH_RELATIONS_LEVEL_WI foreign key (ID_LEVEL_WILAYAH)
      references LEVEL_WILAYAH (ID_LEVEL_WILAYAH)
      on update restrict
      on delete restrict;


package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name = "mutasi_masuk")

public class mutasi_masuk {
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "ID_MUTASI_MASUK", nullable = false)
	private UUID id_mutasi_masuk;
	@Column(name = "NOMOR_REGISTRASI", nullable = false)
	private String nomor_registrasi;
	@Column(name = "TANGGAL_MASUK", nullable = true)
	private DateTime tanggal_masuk;
	@Column(name = "ASAL", nullable = false)
	private String asal;
	
	//Constructor
	public mutasi_masuk(){}

	public mutasi_masuk(String nomor_registrasi, String asal){
		this.setNomor_registrasi(nomor_registrasi);
		this.setAsal(asal);
	}

	public UUID getId_mutasi_masuk() {
		return id_mutasi_masuk;
	}

	public void setId_mutasi_masuk(UUID id_mutasi_masuk) {
		this.id_mutasi_masuk = id_mutasi_masuk;
	}


	public String getNomor_registrasi() {
		return nomor_registrasi;
	}

	public void setNomor_registrasi(String nomor_registrasi) {
		this.nomor_registrasi = nomor_registrasi;
	}

	public DateTime getTanggal_masuk() {
		return tanggal_masuk;
	}

	public void setTanggal_masuk(DateTime tanggal_masuk) {
		this.tanggal_masuk = tanggal_masuk;
	}

	public String getAsal() {
		return asal;
	}

	public void setAsal(String asal) {
		this.asal = asal;
	}
	
}

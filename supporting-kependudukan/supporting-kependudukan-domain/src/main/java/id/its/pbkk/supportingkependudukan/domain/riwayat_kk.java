package id.its.pbkk.supportingkependudukan.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name = "riwayat_kk")
public class riwayat_kk {
	
	@Id 
	@Column(name = "ID_RIWAYAT_KK", nullable = false)
	private int id_riwayat;
	@Column(name = "NOMOR_KK", nullable = false)
	private String nomor_kk;
	@Column(name = "ALAMAT_RIWAYAT_KK", nullable = false)
	private String alamat;
	@Column(name = "TANGGAL_TERBIT_KK", nullable = true)
	private DateTime tanggal_terbit;
	@Column(name = "TEMPAT_TERBIT_KK", nullable = true)
	private String tempat_terbit;
	
	//Constructor
	public riwayat_kk(){}

	public riwayat_kk(int id_riwayat, String nomor_kk, String alamat, String tempat_terbit){
		this.setId_riwayat(id_riwayat);
		this.setNomor_kk(nomor_kk);
		this.setAlamat(alamat);
		this.setTempat_terbit(tempat_terbit);
	}

	public int getId_riwayat() {
		return id_riwayat;
	}

	public void setId_riwayat(int id_riwayat) {
		this.id_riwayat = id_riwayat;
	}

	public String getNomor_kk() {
		return nomor_kk;
	}

	public void setNomor_kk(String nomor_kk) {
		this.nomor_kk = nomor_kk;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public DateTime getTanggal_terbit() {
		return tanggal_terbit;
	}

	public void setTanggal_terbit(DateTime tanggal_terbit) {
		this.tanggal_terbit = tanggal_terbit;
	}

	public String getTempat_terbit() {
		return tempat_terbit;
	}

	public void setTempat_terbit(String tempat_terbit) {
		this.tempat_terbit = tempat_terbit;
	}
	

}

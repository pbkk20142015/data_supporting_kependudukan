package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name = "ktp")
public class ktp {

	
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "NOMOR_REGISTRASI", nullable = false)
	private UUID nomor_registrasi;
	@Column(name = "UNIQUE_ID_WN", nullable = false)
	private String unique_id_wn;
	@Column(name = "ID_MUTASI_MASUK", nullable = true)
	private String id_mutasi_masuk;
	@Column(name = "ALAMAT", nullable = true)
	private String alamat;
	@Column(name = "TANGGAL_TERBIT", nullable = true)
	private DateTime tanggal_terbit;
	@Column(name = "BERLAKU_HINGGA", nullable = true)
	private DateTime berlaku_hingga;
	@Column(name = "FOTO", nullable = true)
	private byte[] foto;
	@Column(name = "STATUS_BERLAKU", nullable = true)
	private int status_berlaku;
	
	
	//Constructor
	public ktp(){}

	public ktp(String unique_id_wn, String alamat, int status_berlaku){
		this.setUnique_id_wn(unique_id_wn);
		this.setAlamat(alamat);
		this.setStatus_berlaku(status_berlaku);
	}



	public String getUnique_id_wn() {
		return unique_id_wn;
	}

	public void setUnique_id_wn(String unique_id_wn) {
		this.unique_id_wn = unique_id_wn;
	}

	public String getId_mutasi_masuk() {
		return id_mutasi_masuk;
	}

	public void setId_mutasi_masuk(String id_mutasi_masuk) {
		this.id_mutasi_masuk = id_mutasi_masuk;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public UUID getNomor_registrasi() {
		return nomor_registrasi;
	}

	public void setNomor_registrasi(UUID nomor_registrasi) {
		this.nomor_registrasi = nomor_registrasi;
	}

	public DateTime getTanggal_terbit() {
		return tanggal_terbit;
	}

	public void setTanggal_terbit(DateTime tanggal_terbit) {
		this.tanggal_terbit = tanggal_terbit;
	}

	public DateTime getBerlaku_hingga() {
		return berlaku_hingga;
	}

	public void setBerlaku_hingga(DateTime berlaku_hingga) {
		this.berlaku_hingga = berlaku_hingga;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public int getStatus_berlaku() {
		return status_berlaku;
	}

	public void setStatus_berlaku(int status_berlaku) {
		this.status_berlaku = status_berlaku;
	}



}

package id.its.pbkk.supportingkependudukan.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "riwayat_warga_negara")
public class riwayat_warga_negara {
	
	@Id 
	@Column(name = "ID_RIWAYAT_WARGA_NEGARA", nullable = false)
	private int id_riwayat;
	@Column(name = "UNIQUE_ID_WN", nullable = false)
	private String unique_id_wn;	
	@Column(name = "NAMA_RIWAYAT", nullable = true)
	private String nama;	
	@Column(name = "JENIS_KELAMIN_RIWAYAT", nullable = true)
	private String jenis_kelamin;
	@Column(name = "GOLONGAN_DARAH_RIWAYAT", nullable = true)
	private String golongan_darah;
	@Column(name = "STATUS_KAWIN_RIWAYAT", nullable = true)
	private String status_kawin;
	@Column(name = "STATUS_HIDUP_RIWAYAT", nullable = true)
	private int status_hidup;
	@Column(name = "STATUS_HUBUNGAN_DALAM_KELUARGA__RIWAYAT", nullable = true)
	private int shdk;
	
	//Constructor
	public riwayat_warga_negara(){}

	public riwayat_warga_negara(int id_riwayat, String unique_id_wn, String nama, String jenis_kelamin, String golongan_darah, String status_kawin, int status_hidup, int shdk){
		this.setId_riwayat(id_riwayat);
		this.setUnique_id_wn(unique_id_wn);
		this.setNama(nama);
		this.setJenis_kelamin(jenis_kelamin);
		this.setGolongan_darah(golongan_darah);
		this.setStatus_kawin(status_kawin);
		this.setStatus_hidup(status_hidup);
		this.setShdk(shdk);
	}

	public int getId_riwayat() {
		return id_riwayat;
	}

	public void setId_riwayat(int id_riwayat) {
		this.id_riwayat = id_riwayat;
	}

	public String getUnique_id_wn() {
		return unique_id_wn;
	}

	public void setUnique_id_wn(String unique_id_wn) {
		this.unique_id_wn = unique_id_wn;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJenis_kelamin() {
		return jenis_kelamin;
	}

	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}

	public String getGolongan_darah() {
		return golongan_darah;
	}

	public void setGolongan_darah(String golongan_darah) {
		this.golongan_darah = golongan_darah;
	}

	public String getStatus_kawin() {
		return status_kawin;
	}

	public void setStatus_kawin(String status_kawin) {
		this.status_kawin = status_kawin;
	}

	public int getStatus_hidup() {
		return status_hidup;
	}

	public void setStatus_hidup(int status_hidup) {
		this.status_hidup = status_hidup;
	}

	public int getShdk() {
		return shdk;
	}

	public void setShdk(int shdk) {
		this.shdk = shdk;
	}

}

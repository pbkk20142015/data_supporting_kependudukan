package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name = "agama")
public class agama {
	
	@Id
	@Column(name = "ID_AGAMA", nullable = false)
	private String id_agama;
	@Column(name = "NAMA_AGAMA", nullable = false)
	private String nama_agama;
	
	//Constructor
	public agama(){}

	public agama(String id_agama, String nama_agama){
		this.setId_agama(id_agama);
		this.setNama_agama(nama_agama);
	}

	public String getId_agama() {
		return id_agama;
	}

	public void setId_agama(String id_agama) {
		this.id_agama = id_agama;
	}

	public String getNama_agama() {
		return nama_agama;
	}

	public void setNama_agama(String nama_agama) {
		this.nama_agama = nama_agama;
	}
	

}

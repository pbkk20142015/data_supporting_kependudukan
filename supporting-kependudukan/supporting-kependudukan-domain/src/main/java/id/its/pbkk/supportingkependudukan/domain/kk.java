package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;


@Entity
@Table(name = "kk")
public class kk {
	
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "NOMOR_KK", nullable = false)
	private UUID nomor_kk;
	@Column(name = "ALAMAT_KK", nullable = false)
	private String alamat;
	@Column(name = "TANGGAL_TERBIT_KK", nullable = true)
	private DateTime tanggal_terbit;
	@Column(name = "TEMPAT_TERBIT_KK", nullable = true)
	private String tempat_terbit;
	
	//Constructor
	public kk(){}

	public kk(String alamat, String tempat_terbit){
		this.setAlamat(alamat);
		this.setTempat_terbit(tempat_terbit);
	}

	public UUID getNomor_kk() {
		return nomor_kk;
	}

	public void setNomor_kk(UUID nomor_kk) {
		this.nomor_kk = nomor_kk;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public DateTime getTanggal_terbit() {
		return tanggal_terbit;
	}

	public void setTanggal_terbit(DateTime tanggal_terbit) {
		this.tanggal_terbit = tanggal_terbit;
	}

	public String getTempat_terbit() {
		return tempat_terbit;
	}

	public void setTempat_terbit(String tempat_terbit) {
		this.tempat_terbit = tempat_terbit;
	}

}

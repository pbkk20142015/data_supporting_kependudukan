package id.its.pbkk.supportingkependudukan.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "level_wilayah")

public class level_wilayah {
	
	@Id 
	@Column(name = "ID_LEVEL_WILAYAH", nullable = false)
	private int id_level_wilayah;
	@Column(name = "NAMA_LEVEL_WILAYAH", nullable = false)
	private String nama_level_wilayah;
	
	//Constructor
	public level_wilayah(){}

	public level_wilayah(int id_level_wilayah, String nama_level_wilayah){
		this.setId_level_wilayah(id_level_wilayah);
		this.setNama_level_wilayah(nama_level_wilayah);
	}

	public int getId_level_wilayah() {
		return id_level_wilayah;
	}

	public void setId_level_wilayah(int id_level_wilayah) {
		this.id_level_wilayah = id_level_wilayah;
	}

	public String getNama_level_wilayah() {
		return nama_level_wilayah;
	}

	public void setNama_level_wilayah(String nama_level_wilayah) {
		this.nama_level_wilayah = nama_level_wilayah;
	}
	
	
}

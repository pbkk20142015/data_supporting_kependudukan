package id.its.pbkk.supportingkependudukan.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pekerjaan")
public class pekerjaan {
	
	@Id
	@Column(name = "ID_PEKERJAAN", nullable = false)
	private String id_pekerjaan;
	@Column(name = "NAMA_PEKERJAAN", nullable = false)
	private String nama_pekerjaan;
	
	//Constructor
	public pekerjaan(){}

	public pekerjaan(String id_pekerjaan, String nama_pekerjaan){
		this.setId_pekerjaan(id_pekerjaan);
		this.setNama_pekerjaan(nama_pekerjaan);
	}

	public String getId_pekerjaan() {
		return id_pekerjaan;
	}

	public void setId_pekerjaan(String id_pekerjaan) {
		this.id_pekerjaan = id_pekerjaan;
	}

	public String getNama_pekerjaan() {
		return nama_pekerjaan;
	}

	public void setNama_pekerjaan(String nama_pekerjaan) {
		this.nama_pekerjaan = nama_pekerjaan;
	}
	
	

}

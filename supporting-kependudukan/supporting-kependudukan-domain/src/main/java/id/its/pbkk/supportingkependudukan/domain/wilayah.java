package id.its.pbkk.supportingkependudukan.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wilayah")
public class wilayah {
	
	@Id
	@Column(name = "ID_WILAYAH", nullable = false)
	private int id_wilayah;
	@Column(name = "WIL_ID_WILAYAH", nullable = false)
	private int wil_id_wilayah;
	@Column(name = "ID_LEVEL_WILAYAH", nullable = false)
	private int id_level_wilayah;
	@Column(name = "NAMA_WILAYAH", nullable = false)
	private String nama_wilayah;
	
	//Constructor
	public wilayah(){}

	public wilayah(int id_wilayah, int wil_id_wilayah, int id_level_wilayah, String nama_wilayah){
		this.setId_wilayah(id_wilayah);
		this.setWil_id_wilayah(wil_id_wilayah);
		this.setId_level_wilayah(id_level_wilayah);
		this.setNama_wilayah(nama_wilayah);
	}

	public int getId_wilayah() {
		return id_wilayah;
	}

	public void setId_wilayah(int id_wilayah) {
		this.id_wilayah = id_wilayah;
	}

	public int getWil_id_wilayah() {
		return wil_id_wilayah;
	}

	public void setWil_id_wilayah(int wil_id_wilayah) {
		this.wil_id_wilayah = wil_id_wilayah;
	}

	public int getId_level_wilayah() {
		return id_level_wilayah;
	}

	public void setId_level_wilayah(int id_level_wilayah) {
		this.id_level_wilayah = id_level_wilayah;
	}

	public String getNama_wilayah() {
		return nama_wilayah;
	}

	public void setNama_wilayah(String nama_wilayah) {
		this.nama_wilayah = nama_wilayah;
	}

}

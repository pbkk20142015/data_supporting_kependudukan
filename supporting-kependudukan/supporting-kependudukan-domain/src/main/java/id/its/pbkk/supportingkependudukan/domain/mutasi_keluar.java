package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name = "mutasi_keluar")

public class mutasi_keluar {
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "ID_MUTASI_MASUK", nullable = false)
	private UUID id_mutasi_keluar;
	@Column(name = "NOMOR_REGISTRASI", nullable = false)
	private String nomor_registrasi;
	@Column(name = "TANGGAL_KELUAR", nullable = true)
	private DateTime tanggal_keluar;
	@Column(name = "TUJUAN", nullable = false)
	private String tujuan;
	
	//Constructor
	public mutasi_keluar(){}

	public mutasi_keluar(String nomor_registrasi, String tujuan){
		this.setNomor_registrasi(nomor_registrasi);
		this.setTujuan(tujuan);
	}

	public String getNomor_registrasi() {
		return nomor_registrasi;
	}

	public void setNomor_registrasi(String nomor_registrasi) {
		this.nomor_registrasi = nomor_registrasi;
	}

	public UUID getId_mutasi_keluar() {
		return id_mutasi_keluar;
	}

	public void setId_mutasi_keluar(UUID id_mutasi_keluar) {
		this.id_mutasi_keluar = id_mutasi_keluar;
	}

	public DateTime getTanggal_keluar() {
		return tanggal_keluar;
	}

	public void setTanggal_keluar(DateTime tanggal_keluar) {
		this.tanggal_keluar = tanggal_keluar;
	}

	public String getTujuan() {
		return tujuan;
	}

	public void setTujuan(String tujuan) {
		this.tujuan = tujuan;
	}
	
}

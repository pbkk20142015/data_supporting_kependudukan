package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;


@Entity
@Table(name = "warga_negara")
public class warga_negara {
	
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "UNIQUE_ID_WN", nullable = false)
	private UUID unique_id_wn;
	@Column(name = "NOMOR_KK", nullable = false)
	private String nomor_kk;
	@Column(name = "ID_PEKERJAAN", nullable = true)
	private String id_pekerjaan;
	@Column(name = "ID_AGAMA", nullable = true)
	private String id_agama;
	@Column(name = "NIK", nullable = true)
	private String nik;
	@Column(name = "NAMA", nullable = true)
	private String nama;
	@Column(name = "TANGGAL_LAHIR", nullable = true)
	private DateTime tanggal_lahir;
	@Column(name = "JENIS_KELAMIN", nullable = true)
	private String jenis_kelamin;
	@Column(name = "GOLONGAN_DARAH", nullable = true)
	private String golongan_darah;
	@Column(name = "STATUS_KAWIN", nullable = true)
	private String status_kawin;
	@Column(name = "STATUS_HIDUP", nullable = true)
	private int status_hidup;
	@Column(name = "SHDK2", nullable = true)
	private int shdk2;
	@Column(name = "NAMA_AYAH", nullable = true)
	private String nama_ayah;
	@Column(name = "NAMA_IBU", nullable = true)
	private String nama_ibu;
	
	//Constructor
	public warga_negara(){}

	public warga_negara(String nomor_kk, String id_pekerjaan, String id_agama, String nik, String nama, String jenis_kelamin, String golongan_darah, String status_kawin, int status_hidup, int shdk2, String nama_ayah, String nama_ibu){
		this.setNomor_kk(nomor_kk);
		this.setId_pekerjaan(id_pekerjaan);
		this.setId_agama(id_agama);
		this.setNik(nik);
		this.setNama(nama);
		this.setJenis_kelamin(jenis_kelamin);
		this.setGolongan_darah(golongan_darah);
		this.setStatus_kawin(status_kawin);
		this.setStatus_hidup(status_hidup);
		this.setShdk2(shdk2);
		this.setNama_ayah(nama_ayah);
		this.setNama_ibu(nama_ibu);
	}

	public UUID getUnique_id_wn() {
		return unique_id_wn;
	}

	public void setUnique_id_wn(UUID unique_id_wn) {
		this.unique_id_wn = unique_id_wn;
	}

	public String getNomor_kk() {
		return nomor_kk;
	}

	public void setNomor_kk(String nomor_kk) {
		this.nomor_kk = nomor_kk;
	}

	public String getId_pekerjaan() {
		return id_pekerjaan;
	}

	public void setId_pekerjaan(String id_pekerjaan) {
		this.id_pekerjaan = id_pekerjaan;
	}

	public String getId_agama() {
		return id_agama;
	}

	public void setId_agama(String id_agama) {
		this.id_agama = id_agama;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public DateTime getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(DateTime tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}

	public String getJenis_kelamin() {
		return jenis_kelamin;
	}

	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}

	public String getGolongan_darah() {
		return golongan_darah;
	}

	public void setGolongan_darah(String golongan_darah) {
		this.golongan_darah = golongan_darah;
	}

	public String getStatus_kawin() {
		return status_kawin;
	}

	public void setStatus_kawin(String status_kawin) {
		this.status_kawin = status_kawin;
	}

	public int getStatus_hidup() {
		return status_hidup;
	}

	public void setStatus_hidup(int status_hidup) {
		this.status_hidup = status_hidup;
	}

	public int getShdk2() {
		return shdk2;
	}

	public void setShdk2(int shdk2) {
		this.shdk2 = shdk2;
	}

	public String getNama_ayah() {
		return nama_ayah;
	}

	public void setNama_ayah(String nama_ayah) {
		this.nama_ayah = nama_ayah;
	}

	public String getNama_ibu() {
		return nama_ibu;
	}

	public void setNama_ibu(String nama_ibu) {
		this.nama_ibu = nama_ibu;
	}

}

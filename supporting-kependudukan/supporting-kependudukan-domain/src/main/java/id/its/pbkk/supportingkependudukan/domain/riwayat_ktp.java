package id.its.pbkk.supportingkependudukan.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

@Entity
@Table(name = "riwayat_ktp")
public class riwayat_ktp {
	
	@Id
	@Column(name = "ID_RIWAYAT_KTP", nullable = false)
	private int id_riwayat;
	@Column(name = "NOMOR_REGISTRASI", nullable = false)
	private String nomor_registrasi;
	@Column(name = "ALAMAT_RIWAYAT", nullable = true)
	private String alamat;
	@Column(name = "TANGGAL_TERBIT", nullable = true)
	private DateTime tanggal_terbit;
	@Column(name = "BERLAKU_HINGGA_RIWAYAT", nullable = true)
	private DateTime berlaku_hingga;
	@Column(name = "FOTO_RIWAYAT", nullable = true)
	private byte[] foto;
	@Column(name = "STATUS_BERLAKU_RIWAYAT", nullable = true)
	private int status_berlaku;
	
	
	//Constructor
	public riwayat_ktp(){}

	public riwayat_ktp(int id_riwayat, String nomor_registrasi, String alamat, int status_berlaku){
		this.setId_riwayat(id_riwayat);
		this.setNomor_registrasi(nomor_registrasi);
		this.setAlamat(alamat);
		this.setStatus_berlaku(status_berlaku);
	}

	public int getId_riwayat() {
		return id_riwayat;
	}

	public void setId_riwayat(int id_riwayat) {
		this.id_riwayat = id_riwayat;
	}

	public String getNomor_registrasi() {
		return nomor_registrasi;
	}

	public void setNomor_registrasi(String nomor_registrasi) {
		this.nomor_registrasi = nomor_registrasi;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public DateTime getTanggal_terbit() {
		return tanggal_terbit;
	}

	public void setTanggal_terbit(DateTime tanggal_terbit) {
		this.tanggal_terbit = tanggal_terbit;
	}

	public DateTime getBerlaku_hingga() {
		return berlaku_hingga;
	}

	public void setBerlaku_hingga(DateTime berlaku_hingga) {
		this.berlaku_hingga = berlaku_hingga;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public int getStatus_berlaku() {
		return status_berlaku;
	}

	public void setStatus_berlaku(int status_berlaku) {
		this.status_berlaku = status_berlaku;
	}

}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.riwayat_warga_negara;

import java.util.List;

public interface riwayat_warga_negaraDAO {

	public List<riwayat_warga_negara> list();
	public riwayat_warga_negara findById(String id);
	public void save(riwayat_warga_negara data);
	public void update(riwayat_warga_negara data);
}

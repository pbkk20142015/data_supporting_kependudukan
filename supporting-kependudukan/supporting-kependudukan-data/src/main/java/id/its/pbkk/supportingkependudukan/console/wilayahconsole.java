package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.wilayahDAO;
import id.its.pbkk.supportingkependudukan.domain.wilayah;

import org.springframework.context.support.GenericXmlApplicationContext;

public class wilayahconsole {
	
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        wilayah data_1 = new wilayah(1, 1, 1, "Jawa Timur");
        wilayahDAO controller = ctx.getBean("wilayah",wilayahDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }

}

package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.pekerjaanDAO;
import id.its.pbkk.supportingkependudukan.domain.pekerjaan;

import org.springframework.context.support.GenericXmlApplicationContext;

public class pekerjaanconsole {
	
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        pekerjaan data_1 = new pekerjaan("PLJ", "Pelajar");
        pekerjaanDAO controller = ctx.getBean("pekerjaan",pekerjaanDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }

}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.riwayat_kk;

import java.util.List;

public interface riwayat_kkDAO {
	
	public List<riwayat_kk> list();
	public riwayat_kk findById(String id);
	public void save(riwayat_kk data);
	public void update(riwayat_kk data);

}

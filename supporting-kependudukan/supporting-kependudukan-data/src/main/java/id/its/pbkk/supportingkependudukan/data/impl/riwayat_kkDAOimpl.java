package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.riwayat_kkDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_kk;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("riwayat_kk")
public class riwayat_kkDAOimpl implements riwayat_kkDAO {
	
	public riwayat_kkDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public List<riwayat_kk> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public riwayat_kk findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(riwayat_kk data) {
		// TODO Auto-generated method stub
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close();
	}

	public void update(riwayat_kk data) {
		// TODO Auto-generated method stub
		
	}

}

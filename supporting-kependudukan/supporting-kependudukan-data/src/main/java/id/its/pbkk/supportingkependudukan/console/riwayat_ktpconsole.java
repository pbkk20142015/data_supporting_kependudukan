package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.riwayat_ktpDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_ktp;

import org.springframework.context.support.GenericXmlApplicationContext;

public class riwayat_ktpconsole {
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        riwayat_ktp data_1 = new riwayat_ktp(1, "9ff5bdd8-096f-11e5-b31d-6c71d92ce20c", "Jl mawar", 2);
        riwayat_ktpDAO controller = ctx.getBean("riwayat_ktp",riwayat_ktpDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }

}

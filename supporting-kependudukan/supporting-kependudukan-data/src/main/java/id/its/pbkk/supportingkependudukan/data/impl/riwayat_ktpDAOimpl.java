package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.riwayat_ktpDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_ktp;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("riwayat_ktp")
public class riwayat_ktpDAOimpl implements riwayat_ktpDAO {
	
	public riwayat_ktpDAOimpl(){}
    //SessionFactory
    	private SessionFactory session_factory;
	
	@Resource(name="sessionFactory") 
        public void setSessionFactory(SessionFactory sessionFactory){
            this.session_factory = sessionFactory;
        }
    
        public SessionFactory getSessionFactory(){
            return this.session_factory;
        }

		public List<riwayat_ktp> list() {
			// TODO Auto-generated method stub
			return null;
		}

		public riwayat_ktp findById(String id) {
			// TODO Auto-generated method stub
			return null;
		}

		public void save(riwayat_ktp data) {
			// TODO Auto-generated method stub
			Session session = this.session_factory.openSession();
	        Transaction tx = session.beginTransaction();
	        session.persist(data);
	        tx.commit();
	        session.close();
		}

		public void update(riwayat_ktp data) {
			// TODO Auto-generated method stub
			
		}

}

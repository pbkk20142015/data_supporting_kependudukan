package id.its.pbkk.supportingkependudukan.data;

import java.util.List;

import id.its.pbkk.supportingkependudukan.domain.ktp;

public interface ktpDAO {
	
	public List<ktp> list();
	public ktp findById(String id);
	public void save(ktp data);
	public void update(ktp data);

}

package id.its.pbkk.supportingkependudukan.data;


import id.its.pbkk.supportingkependudukan.domain.warga_negara;

import java.util.List;

public interface warga_negaraDAO {

	public List<warga_negara> list();
	public warga_negara findById(String id);
	public void save(warga_negara data);
	public void update(warga_negara data);
}

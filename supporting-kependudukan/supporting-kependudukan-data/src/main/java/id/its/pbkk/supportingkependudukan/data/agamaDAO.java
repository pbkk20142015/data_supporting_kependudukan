package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.agama;

import java.util.List;

public interface agamaDAO {
	
	public List<agama> list();
	public agama findById(String id);
	public void save(agama data);
	public void update(agama data);

}

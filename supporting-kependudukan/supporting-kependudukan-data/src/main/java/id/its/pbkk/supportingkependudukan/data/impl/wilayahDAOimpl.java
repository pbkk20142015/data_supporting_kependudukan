package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.wilayahDAO;
import id.its.pbkk.supportingkependudukan.domain.wilayah;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("wilayah")
public class wilayahDAOimpl implements wilayahDAO {
	
	public wilayahDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<wilayah> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public wilayah findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(wilayah data) {
		// TODO Auto-generated method stub
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close();
	}

	public void update(wilayah data) {
		// TODO Auto-generated method stub
		
	}

}

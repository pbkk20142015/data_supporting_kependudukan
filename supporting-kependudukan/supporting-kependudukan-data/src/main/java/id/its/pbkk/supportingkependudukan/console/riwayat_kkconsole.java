package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.riwayat_kkDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_kk;

import org.springframework.context.support.GenericXmlApplicationContext;

public class riwayat_kkconsole {
	
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        riwayat_kk data_1 = new riwayat_kk(1, "f7e4eb8e-096d-11e5-b31d-6c71d92ce20c", "jl.pisang", "surabaya");
        riwayat_kkDAO controller = ctx.getBean("riwayat_kk",riwayat_kkDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }

}

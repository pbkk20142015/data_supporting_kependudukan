package id.its.pbkk.supportingkependudukan.console;


import id.its.pbkk.supportingkependudukan.data.mutasi_masukDAO;
import id.its.pbkk.supportingkependudukan.domain.mutasi_masuk;

import org.springframework.context.support.GenericXmlApplicationContext;

public class mutasi_masukconsole {

	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        mutasi_masuk data_1 = new mutasi_masuk("9ff5bdd8-096f-11e5-b31d-6c71d92ce20c", "Surabaya");
        mutasi_masukDAO controller = ctx.getBean("mutasi_masuk",mutasi_masukDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

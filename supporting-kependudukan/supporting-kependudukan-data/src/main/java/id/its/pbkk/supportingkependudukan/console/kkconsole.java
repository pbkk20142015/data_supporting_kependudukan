package id.its.pbkk.supportingkependudukan.console;


import id.its.pbkk.supportingkependudukan.data.kkDAO;
import id.its.pbkk.supportingkependudukan.domain.kk;

import org.springframework.context.support.GenericXmlApplicationContext;

public class kkconsole {

	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        kk data_1 = new kk("jl pisang", "surabaya");
        kkDAO controller = ctx.getBean("kk",kkDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import id.its.pbkk.supportingkependudukan.data.ktpDAO;
import id.its.pbkk.supportingkependudukan.domain.ktp;



@Transactional
@Repository("ktp")
public class ktpDAOimpl implements ktpDAO{
	
	public ktpDAOimpl(){}
    //SessionFactory
    	private SessionFactory session_factory;
	
	@Resource(name="sessionFactory") 
        public void setSessionFactory(SessionFactory sessionFactory){
            this.session_factory = sessionFactory;
        }
    
        public SessionFactory getSessionFactory(){
            return this.session_factory;
        }

		public List<ktp> list() {
			// TODO Auto-generated method stub
			return null;
		}

		public ktp findById(String id) {
			// TODO Auto-generated method stub
			return null;
		}

		public void save(ktp data) {
			Session session = this.session_factory.openSession();
	        Transaction tx = session.beginTransaction();
	        session.persist(data);
	        tx.commit();
	        session.close();  
		}

		public void update(ktp data) {
			session_factory.getCurrentSession().update(data);
			
		}

}

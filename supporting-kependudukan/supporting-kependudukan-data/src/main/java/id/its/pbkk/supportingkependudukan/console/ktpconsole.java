package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.ktpDAO;
import id.its.pbkk.supportingkependudukan.domain.ktp;

import org.springframework.context.support.GenericXmlApplicationContext;

public class ktpconsole {
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        ktp data_1 = new ktp("e1c41f98-096c-11e5-b31d-6c71d92ce20c","Jl mawar",2);
        ktpDAO controller = ctx.getBean("ktp",ktpDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

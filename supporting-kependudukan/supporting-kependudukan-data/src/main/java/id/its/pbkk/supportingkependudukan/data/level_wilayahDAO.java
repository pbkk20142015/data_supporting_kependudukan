package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.level_wilayah;

import java.util.List;

public interface level_wilayahDAO {
	public List<level_wilayah> list();
	public level_wilayah findById(String id);
	public void save(level_wilayah data);
	public void update(level_wilayah data);
}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.mutasi_keluar;

import java.util.List;


public interface mutasi_keluarDAO {
	public List<mutasi_keluar> list();
	public mutasi_keluar findById(String id);
	public void save(mutasi_keluar data);
	public void update(mutasi_keluar data);
}

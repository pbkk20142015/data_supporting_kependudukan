package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.mutasi_keluarDAO;
import id.its.pbkk.supportingkependudukan.domain.mutasi_keluar;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository("mutasi_keluar")
public class mutasi_keluarDAOimpl implements mutasi_keluarDAO {
	public mutasi_keluarDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<mutasi_keluar> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public mutasi_keluar findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(mutasi_keluar data) {
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close(); 
		
	}

	public void update(mutasi_keluar data) {
		// TODO Auto-generated method stub
		
	}


}

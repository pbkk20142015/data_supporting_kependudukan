package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.supportingkependudukan.data.agamaDAO;
import id.its.pbkk.supportingkependudukan.domain.agama;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("agama")
public class agamaDAOimpl implements agamaDAO {
	
	public agamaDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<agama> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public agama findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(agama data) {
		// TODO Auto-generated method stub
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close();
	}

	public void update(agama data) {
		// TODO Auto-generated method stub
		
	}

}

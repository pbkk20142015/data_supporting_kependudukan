package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.kkDAO;
import id.its.pbkk.supportingkependudukan.domain.kk;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository("kk")
public class kkDAOimpl implements kkDAO {

	public kkDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<kk> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public kk findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(kk data) {
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close(); 
	}

	public void update(kk data) {
		// TODO Auto-generated method stub

	}

}

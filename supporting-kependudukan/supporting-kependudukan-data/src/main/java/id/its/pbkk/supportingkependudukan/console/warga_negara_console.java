package id.its.pbkk.supportingkependudukan.console;


import id.its.pbkk.supportingkependudukan.data.warga_negaraDAO;
import id.its.pbkk.supportingkependudukan.domain.warga_negara;

import org.springframework.context.support.GenericXmlApplicationContext;

public class warga_negara_console {
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        warga_negara data_1 = new warga_negara("f7e4eb8e-096d-11e5-b31d-6c71d92ce20c","PLJ","IS","412213","icha","P","O","1",3,2,"papa","mama");
        warga_negaraDAO controller = ctx.getBean("warga_negara",warga_negaraDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.kk;

import java.util.List;

public interface kkDAO {

	public List<kk> list();
	public kk findById(String id);
	public void save(kk data);
	public void update(kk data);
}

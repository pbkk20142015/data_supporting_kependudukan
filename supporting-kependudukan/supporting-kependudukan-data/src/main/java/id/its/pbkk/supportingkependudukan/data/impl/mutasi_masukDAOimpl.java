package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.mutasi_masukDAO;
import id.its.pbkk.supportingkependudukan.domain.mutasi_masuk;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("mutasi_masuk")
public class mutasi_masukDAOimpl implements mutasi_masukDAO {
	public mutasi_masukDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<mutasi_masuk> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public mutasi_masuk findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(mutasi_masuk data) {
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close(); 
		
	}

	public void update(mutasi_masuk data) {
		// TODO Auto-generated method stub
		
	}


	
}

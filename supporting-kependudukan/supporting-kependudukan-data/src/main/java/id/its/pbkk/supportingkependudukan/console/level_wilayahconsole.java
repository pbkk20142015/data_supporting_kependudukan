package id.its.pbkk.supportingkependudukan.console;

import org.springframework.context.support.GenericXmlApplicationContext;

import id.its.pbkk.supportingkependudukan.data.level_wilayahDAO;
import id.its.pbkk.supportingkependudukan.domain.level_wilayah;

public class level_wilayahconsole {
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        level_wilayah data_1 = new level_wilayah(1, "Negara");
        level_wilayahDAO controller = ctx.getBean("level_wilayah",level_wilayahDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

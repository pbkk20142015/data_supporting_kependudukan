package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.riwayat_warga_negaraDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_warga_negara;

import org.springframework.context.support.GenericXmlApplicationContext;

public class riwayat_warga_negaraconsole {

	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        riwayat_warga_negara data_1 = new riwayat_warga_negara(3001,"e1c41f98-096c-11e5-b31d-6c71d92ce20c","icha", "P","O","1",3,2);
        riwayat_warga_negaraDAO controller = ctx.getBean("riwayat_warga_negara",riwayat_warga_negaraDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.ktp;
import id.its.pbkk.supportingkependudukan.domain.riwayat_ktp;

import java.util.List;

public interface riwayat_ktpDAO {
	
	public List<riwayat_ktp> list();
	public riwayat_ktp findById(String id);
	public void save(riwayat_ktp data);
	public void update(riwayat_ktp data);

}

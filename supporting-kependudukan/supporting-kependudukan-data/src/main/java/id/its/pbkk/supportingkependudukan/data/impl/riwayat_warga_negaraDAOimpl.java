package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.riwayat_warga_negaraDAO;
import id.its.pbkk.supportingkependudukan.domain.riwayat_warga_negara;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository("riwayat_warga_negara")
public class riwayat_warga_negaraDAOimpl implements riwayat_warga_negaraDAO{
	
	public riwayat_warga_negaraDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<riwayat_warga_negara> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public riwayat_warga_negara findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(riwayat_warga_negara data) {
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close(); 
		
	}

	public void update(riwayat_warga_negara data) {
		// TODO Auto-generated method stub
		
	}
	

}

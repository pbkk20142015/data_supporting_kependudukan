package id.its.pbkk.supportingkependudukan.data;

import java.util.List;

import id.its.pbkk.supportingkependudukan.domain.mutasi_masuk;

public interface mutasi_masukDAO {
	public List<mutasi_masuk> list();
	public mutasi_masuk findById(String id);
	public void save(mutasi_masuk data);
	public void update(mutasi_masuk data);
}

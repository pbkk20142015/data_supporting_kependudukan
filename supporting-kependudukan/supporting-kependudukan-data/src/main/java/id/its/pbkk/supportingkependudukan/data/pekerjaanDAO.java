package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.pekerjaan;

import java.util.List;

public interface pekerjaanDAO {
	
	public List<pekerjaan> list();
	public pekerjaan findById(String id);
	public void save(pekerjaan data);
	public void update(pekerjaan data);

}

package id.its.pbkk.supportingkependudukan.data;

import id.its.pbkk.supportingkependudukan.domain.wilayah;

import java.util.List;

public interface wilayahDAO {
	
	public List<wilayah> list();
	public wilayah findById(String id);
	public void save(wilayah data);
	public void update(wilayah data);

}

package id.its.pbkk.supportingkependudukan.console;

import id.its.pbkk.supportingkependudukan.data.agamaDAO;
import id.its.pbkk.supportingkependudukan.domain.agama;

import org.springframework.context.support.GenericXmlApplicationContext;

public class agamaconsole {
	
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        agama data_1 = new agama("IS", "Islam");
        agamaDAO controller = ctx.getBean("agama",agamaDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }

}

package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.level_wilayahDAO;
import id.its.pbkk.supportingkependudukan.domain.level_wilayah;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("level_wilayah")

public class level_wilayahDAOimpl implements level_wilayahDAO {
	public level_wilayahDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<level_wilayah> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public level_wilayah findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(level_wilayah data) {
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close(); 
		
	}

	public void update(level_wilayah data) {
		// TODO Auto-generated method stub
		
	}
}

package id.its.pbkk.supportingkependudukan.data.impl;

import java.util.List;

import id.its.pbkk.supportingkependudukan.data.pekerjaanDAO;
import id.its.pbkk.supportingkependudukan.domain.pekerjaan;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("pekerjaan")
public class pekerjaanDAOimpl implements pekerjaanDAO {
	
	public pekerjaanDAOimpl(){}
	//SessionFactory
	private SessionFactory session_factory;

	@Resource(name="sessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory){
		this.session_factory = sessionFactory;
	}

	public SessionFactory getSessionFactory(){
		return this.session_factory;
	}

	public List<pekerjaan> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public pekerjaan findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(pekerjaan data) {
		// TODO Auto-generated method stub
		Session session = this.session_factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(data);
        tx.commit();
        session.close();
	}

	public void update(pekerjaan data) {
		// TODO Auto-generated method stub
		
	}

}

package id.its.pbkk.supportingkependudukan.console;

import org.springframework.context.support.GenericXmlApplicationContext;

import id.its.pbkk.supportingkependudukan.data.mutasi_keluarDAO;
import id.its.pbkk.supportingkependudukan.domain.mutasi_keluar;

public class mutasi_keluarconsole {
	
	public static void main(String[] args) 
    {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        
        mutasi_keluar data_1 = new mutasi_keluar("9ff5bdd8-096f-11e5-b31d-6c71d92ce20c", "Jakarta");
        mutasi_keluarDAO controller = ctx.getBean("mutasi_keluar",mutasi_keluarDAO.class);
        controller.save(data_1);
        controller.list();
        System.out.println("Lihat database Mysql anda");
    }
}
